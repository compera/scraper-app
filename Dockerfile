FROM python:3.6.4-alpine3.7

WORKDIR /usr/src/app

COPY requirements.txt ./

# RUN apk update and apk add
# RUN apt-get update && apt-get install python-dev
    

RUN apk add --update --no-cache --virtual .pynacl_deps build-base python3-dev libffi-dev libressl-dev libxml2-dev xmlsec-dev
RUN pip install --no-cache-dir -r requirements.txt

# CMD "/bin/bash"