# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ProductBasicInfo(scrapy.Item):
    _id = scrapy.Field()
    store = scrapy.Field()
    product_model_id = scrapy.Field()
    url = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()


class ProductPeriodicInfo(scrapy.Item):
    _product_id = scrapy.Field()
    _time_key = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    rating = scrapy.Field()


class ProductPeriodicVariations(scrapy.Item):
    _product_id = scrapy.Field()
    _time_key = scrapy.Field()
    model_vari_id = scrapy.Field()
    list = scrapy.Field()

class ModelXpathKeys(scrapy.Item):
    _model_id = scrapy.Field()
    main_xpath_key = scrapy.Field()
    valid = scrapy.Field()
    store = scrapy.Field()


class ProductPageSrc(scrapy.Item):
    _id = scrapy.Field()
    product_id = scrapy.Field()
    html_src = scrapy.Field()

class ProductsPagesListsInitial(scrapy.Item):
    _id = scrapy.Field()
    root_url = scrapy.Field()
    store = scrapy.Field()
    product_type = scrapy.Field()
    interval_start_from = scrapy.Field()
    total_pages = scrapy.Field()
    total_interval = scrapy.Field()
    pagination_baseurl = scrapy.Field()
    page_interval = scrapy.Field()
    pagination_interval = scrapy.Field()
    links = scrapy.Field()


class PagesLinks(scrapy.Item):
    index = scrapy.Field()
    root_id = scrapy.Field()
    page_url = scrapy.Field()
    links = scrapy.Field()


class ProductsPagesListsSrc(scrapy.Item):
    _id = scrapy.Field()
    list_id = scrapy.Field()
    html_src = scrapy.Field()


class CrawlerItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
