# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
import logging
from scrapy.conf import settings

class MongoDBPipeline(object):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        self.db = connection[settings['MONGODB_DB']]

    def process_item(self, item, spider):
        process_type = type(item).__name__.lower()

        if process_type == "productbasicinfo":
            collection = self.db["products"]
            collection.update_one(
                {
                    "_id": item["_id"]
                },
                {
                    "$set": {
                        "store": item["store"],
                        "product_model_id": item["product_model_id"],
                        "url": item["url"],
                        "image_urls": item["image_urls"],
                        "images": item["images"],
                    }
                },
                True
            )
            logging.debug("Product added to database!")

            return item

        if process_type == "productperiodicinfo":
            product_id = item["_product_id"]
            time_key = item["_time_key"]
            collection = self.db["products"]
            collection.update_one(
                {'_id': product_id},
                {
                    "$set": {
                        "last_updated": time_key,
                        "periodic." + time_key: {
                            "title": item["title"],
                            "price": item["price"],
                            "rating": item["rating"],
                            "variations": []
                        }
                    }
                },
                True
            )
            logging.debug("Periodic info updated!")

            return item

        if process_type == "productperiodicvariations":
            product_id = item["_product_id"]
            time_key = item["_time_key"]
            collection = self.db["products"]
            collection.update_one(
                {'_id': product_id},
                {
                    "$push": {
                        "periodic." + time_key + ".variations": {
                            "model_vari_id": item["model_vari_id"],
                            "list": item["list"]
                        }
                    }
                }
            )
            logging.debug("On page variations info updated!")

            return item

        if process_type == "modelxpathkeys":
            model_id = item["_model_id"]
            main_xpath_key = item["main_xpath_key"]
            collection = self.db["product_models"]
            collection.update_one(
                {'_id': model_id},
                {
                    "$set": {
                        "variationsXpathKeys." + main_xpath_key: {
                            "valid": item["valid"],
                            "store": item["store"]
                        }
                    }
                }
            )
            logging.debug("Model xpaths info updated!")

            return item

        if process_type == "productpagesrc":
            collection = self.db["product_page_src"]
            collection.replace_one(
                {'_id': item['_id']}, dict(item), upsert=True)
            logging.debug("Product page source added to database!")

        if process_type == "productspageslistsinitial":
            collection = self.db["products_pages_lists"]
            collection.replace_one(
                {'_id': item['_id']}, dict(item), upsert=True)
            logging.debug("Products pages lists added to database!")
            return item

        if process_type == "pageslinks":
            collection = self.db["products_pages_lists"]
            root_id = item['root_id']
            collection.update(
                {'_id': root_id},
                {
                    '$push': {
                        "links": {
                            "index": item['index'],
                            "page_url": item['page_url'],
                            "links": item['links'],
                        },
                    }
                },
                True
            )
            logging.debug("pages links added to database!")
            return item

        if process_type == "productspageslistssrc":
            collection = self.db["products_pages_lists_src"]
            collection.replace_one(
                {'_id': item['_id']}, dict(item), upsert=True)
            logging.debug("Products pages lists src added to database!")
