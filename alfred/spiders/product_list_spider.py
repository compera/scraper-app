import scrapy
from scrapy_splash import SplashRequest
from urllib.parse import urlsplit
from alfred.items import (
    ProductsPagesListsInitial,
    PagesLinks,
    # ProductsPagesListsSrc,
)
from data_utils import data_utils
from html_process.product_list_page import product_list_page

script = """
    -- main script
    function main(splash)
      local addPosition = splash:jsfunc([[
        function() {
          var allNodes = document.querySelectorAll("*")

          for (let i=0; i < allNodes.length - 1; i++) {
              try {
                allNodes[i].setAttribute(
                    "pos",
                    JSON.stringify(allNodes[i].getBoundingClientRect())
                );
                let border = {
                    "px": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("border-top-width")
                    ),
                    "color": (
                         window.getComputedStyle(allNodes[i], null)
                         .getPropertyValue("border-bottom-color")
                    ),
                    "style": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("border-right-style")
                    )
                }
                let al_style = {
                    "color": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("color")
                    ),
                    "font-size": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("font-size")
                    ),
                    "text-decoration": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("text-decoration")
                    ),
                    "font-weight": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("font-weight")
                    ),
                    "cursor": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("cursor")
                    ),
                    "background-color":  (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("background-color")
                    ),
                    "border": border,
                }
                allNodes[i].setAttribute(
                    "pos",
                    JSON.stringify(allNodes[i].getBoundingClientRect())
                );
                allNodes[i].setAttribute("al_style", JSON.stringify(al_style));
              }
              catch (e) {
                 pass;
              }
          }
        }
      ]])


      assert(splash:go(splash.args.url))
      assert(splash:wait(0.5))

      splash:set_viewport_size(1980, 8020)

      addPosition()

      local html = splash:html()
      return html
    end
    """


class ProductListSpider(scrapy.Spider):
    name = "product_list_spider"

    def start_requests(self):
        urls = [
            # "https://www.chewy.com/b/dry-food-294",
            # "https://www.petsmart.com/dog/food/dry-food/?srule=best-sellers&pmin=0&sz=24&start=0"
            "https://www.petco.com/shop/en/petcostore/category/dog/dog-food/dry-dog-food",
            # "https://www.petsupplies.com/dog-supplies/food/dry-dog-food/9359/"
            # "https://www.drsfostersmith.com/dog-supplies/food/dry-food/ps/c/3307/28497/28498"
        ]

        for url in urls:
            yield SplashRequest(
                url,
                self.parse,
                endpoint='execute',
                args={
                    'lua_source': script,
                    'timeout': 1800,
                    # 'proxy': 'tor',
                },
            )

    def parse(self, response):
        url = response.url
        html = response.body

        list_product_type = "dry dog food"

        products_pages_lists_init = ProductsPagesListsInitial()
        store = data_utils.get_baseurl(url)
        products_pages_lists_init["store"] = store
        products_pages_lists_init["_id"] = (
            store +
            "_" + list_product_type.replace(" ", "_")
        )
        products_pages_lists_init["product_type"] = list_product_type
        products_pages_lists_init["root_url"] = url

        # products_pages_lists_src = ProductsPagesListsSrc()
        # products_pages_lists_src["_id"] = (
        #     "src_" + store + "_" +
        #     list_product_type.replace(" ", "_")
        # )
        # products_pages_lists_src["list_id"] = (
        #     store +
        #     "_" + list_product_type.replace(" ", "_")
        # )

        #
        # html process
        #
        urlsplittd = urlsplit(url)
        domain = urlsplittd.scheme + "://" + urlsplittd.netloc
        html_to_process = (
            html.decode('utf-8')
            .replace("\\n", '')
            .replace("\\t", '')
            .replace("\n", '')
            .replace("\t", '')
        )
        # products_pages_lists_src["html_src"] = html_to_process
        # yield products_pages_lists_src

        soup = product_list_page.run(html_to_process)
        boxes_container_xpath = (
            product_list_page.get_boxes_container_xpath(soup)
        )

        #
        # PAGINATION
        #
        pagination_info = product_list_page.pagination_process_info(soup)
        products_pages_lists_init["interval_start_from"] = (
            pagination_info["interval_start_from"]
        )
        products_pages_lists_init["total_pages"] = (
            pagination_info["total_pages"]
        )
        products_pages_lists_init["total_interval"] = (
            pagination_info["total_interval"]
        )
        products_pages_lists_init["pagination_baseurl"] = (
            pagination_info["baseurl"]
        )
        products_pages_lists_init["page_interval"] = (
            pagination_info["page_interval"]
        )

        links_index = 1

        links_obj = self.prep_links_data(
            links_index,
            url,
            domain,
            response,
            boxes_container_xpath
        )

        products_pages_lists_init["links"] = [links_obj]
        yield products_pages_lists_init

        pagination_interval = range(
            int(pagination_info["interval_start_from"]),
            int(pagination_info["total_interval"]) +
            int(pagination_info["page_interval"]),
            int(pagination_info["page_interval"])
        )
        pages_index = enumerate(pagination_interval, links_index + 1)

        for idx, interval in pages_index:
            next_url = (
                pagination_info["baseurl"].replace("TO_REPLACE", str(interval))
                if domain in pagination_info["baseurl"]
                else
                domain +
                pagination_info["baseurl"].replace("TO_REPLACE", str(interval))
            )

            if idx == 1:
                return
            print(idx)
            # request = scrapy.Request(
            #     next_url,
            #     self.parse_next_page,
            #     meta={
            #         'splash': {
            #             'endpoint': 'execute',
            #             'args': {
            #                 'lua_source': script,
            #                 'timeout': 1800,
            #                 'proxy': 'tor',
            #             },
            #         }
            #     }
            # )
            # request.meta['domain'] = domain
            # request.meta['index'] = idx
            # request.meta["root_id"] = products_pages_lists_init["_id"]

            # yield request

    def parse_next_page(self, response):
        domain = response.meta['domain']
        index = response.meta['index']
        root_id = response.meta["root_id"]

        url = response.url
        html = response.body

        html_to_process = (
            html.decode('utf-8')
            .replace("\\n", '')
            .replace("\\t", '')
            .replace("\n", '')
            .replace("\t", '')
        )
        soup = product_list_page.run(html_to_process)
        boxes_container_xpath = (
            product_list_page.get_boxes_container_xpath(soup)
        )
        links_obj = self.prep_links_data(
            index,
            url,
            domain,
            response,
            boxes_container_xpath
        )
        pages_links = PagesLinks()
        pages_links["root_id"] = root_id
        pages_links["index"] = links_obj["index"]
        pages_links["page_url"] = links_obj["page_url"]
        pages_links["links"] = links_obj["links"]

        yield pages_links

    def prep_links_data(self, index, page_url, domain, res, boxes_xpath):
        links_obj = {}
        links_obj["index"] = index
        links_obj["page_url"] = page_url
        links_obj["links"] = []

        links_holder = []
        for href in res.xpath(boxes_xpath + '//@href').extract():
            links_holder.append(domain + href)

        # remove duplictes links
        links_obj["links"] = list(set(links_holder))

        return links_obj
