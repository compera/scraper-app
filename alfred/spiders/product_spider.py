"""Product page scraper."""
import scrapy
import pymongo
from scrapy.conf import settings
from scrapy.utils.response import get_base_url
from scrapy_splash import SplashRequest
# from alfred.items import (
#     # ProductObject,
#     # ProductPageSrc
# )
import json
import re
from data_utils import data_utils
from html_process.product_page import product_page
from product_model import product_model as promodel
from product_model import product_model2 as pro_model

script = """
    -- main script
    function main(splash)
      local addPosition = splash:jsfunc([[
        function() {
          var allNodes = document.querySelectorAll("*")

          for (let i=0; i < allNodes.length - 1; i++) {
              try {
                allNodes[i].setAttribute("pos", JSON.stringify(
                    allNodes[i].getBoundingClientRect()));
                let border = {
                    "px": (
                      window.getComputedStyle(allNodes[i], null)
                      .getPropertyValue("border-top-width")
                    ),
                    "color": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("border-bottom-color")
                    ),
                    "style": (
                      window.getComputedStyle(allNodes[i], null)
                      .getPropertyValue("border-right-style")
                    )
                }

                let al_style = {
                    "color": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("color")
                    ),
                    "font-size": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("font-size")
                    ),
                    "text-decoration": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("text-decoration")
                    ),
                    "font-weight": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("font-weight")
                    ),
                    "cursor": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("cursor")
                    ),
                    "background-color": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("background-color")
                    ),
                    "border": border,
                }
                allNodes[i].setAttribute("pos", JSON.stringify(
                    allNodes[i].getBoundingClientRect()));
                allNodes[i].setAttribute("al_style", JSON.stringify(al_style));
              }
              catch (e) {
                 pass;
              }
          }
        }
      ]])
      --splash:on_request(function(request)
      --  request:set_proxy{
      --      host = "tor",
      --      port = 9050,
      --      type = 'SOCKS5'
      --  }
      --end)
      assert(splash:go(splash.args.url))
      assert(splash:wait(1.5))

      splash:set_viewport_size(1980, 8020)

      addPosition()

      local html = splash:html()
      return html
    end
    """
product_variation_script = """
    -- main script
    function main(splash)

        local get_innerText_by_xpath = splash:jsfunc([[
            function(xpath) {
              var element = document.evaluate(
                  xpath,
                  document,
                  null,
                  XPathResult.ANY_TYPE, null
              )
              var element_node, element_nodes = []
              while (element_node = element.iterateNext()) {
                element_nodes.push(element_node);
              }

              return (
                  element_nodes[0] ? element_nodes[0].innerText : 'undefined'
              )
            }
        ]])

        local get_element_by_xpath = splash:jsfunc([[
            function(xpath) {
              var element = document.evaluate(
                  xpath,
                  document,
                  null,
                  XPathResult.ANY_TYPE,
                  null
              )
              var element_node, element_nodes = []
              while (element_node = element.iterateNext()) {
                element_nodes.push(element_node);
              }
              return element_nodes.length > 0 ? element_nodes[0] : 'undefined'
            }
        ]])

        local click_on_element = splash:jsfunc([[
            function(xpath) {
                var element = document.evaluate(
                    xpath,
                    document,
                    null,
                    XPathResult.ANY_TYPE,
                    null
                )
                var element_node, element_nodes = []
                while (element_node = element.iterateNext()) {
                     element_nodes.push(element_node);
                }
                var clickable_element = element_nodes[0] ? element_nodes[0] : false

                if(clickable_element) {
                    clickable_element.click()
                    if (clickable_element.children.length > 0) {
                        clickable_element.children[0].click()
                    }
                }
              }
        ]])
        local click_on_element_children = splash:jsfunc([[
            function(xpath) {
                var element = document.evaluate(
                    xpath,
                    document,
                    null,
                    XPathResult.ANY_TYPE,
                    null
                )
                var element_node, element_nodes = []
                while (element_node = element.iterateNext()) {
                     element_nodes.push(element_node);
                }
                var clickable_element = element_nodes[0]
                if (clickable_element.children.length > 0) {
                    clickable_element.children[0].click()
                }
              }
        ]])
        local title_selector = splash.args.main_title_xpath
        local price_selector = splash.args.product_price_xpath
        local product_variation_holder_xpath = splash.args.product_variation_holder_xpath
        local product_variation_selectors = splash.args.product_variation

        local main_vari_element = get_element_by_xpath(product_variation_holder_xpath)
        local vari_is_options = false
        if main_vari_element.options then
            vari_is_options = true
        end

        assert(splash:go(splash.args.url, splash.args.baseurl))
        assert(splash:wait(2.5))

        local results = {}
        for i, v in ipairs( product_variation_selectors ) do
            local variation_selector = product_variation_selectors[i]
            if get_element_by_xpath(variation_selector) ~= "undefined" then
                obj = {}
                assert(splash:wait(2))
                if vari_is_options then
                    --- local main_bounds =
                    local select_element = get_element_by_xpath(variation_selector)
                    local select_bounds = select_element:bounds()
                    --- assert(main_vari_element:mouse_click{x=main_bounds.width/3, y=main_bounds.height/3})
                    assert(select_element:mouse_click{x=select_bounds.width/3, y=select_bounds.height/3})
                else
                    click_on_element(variation_selector)
                end
                assert(splash:wait(4)) 
                obj["title"] = get_innerText_by_xpath(title_selector)
                obj["price"] = get_innerText_by_xpath(price_selector)
                obj["variation"] = get_innerText_by_xpath(variation_selector)

                ---try another click on child element
                local child_click_check = (
                    obj["title"] == "undefined" or
                    obj["price"] == "undefined" or
                    obj["variation"] == "undefined"
                )
                if child_click_check then
                    click_on_element_children(variation_selector)
                    assert(splash:wait(4))
                    obj["title"] = get_innerText_by_xpath(title_selector)
                    obj["price"] = get_innerText_by_xpath(price_selector)
                    obj["variation"] = get_innerText_by_xpath(
                        variation_selector
                    )
                end
                results[#results+1] = obj
            end
        end
      return {results=results}
    end
    """

class ProductSpider(scrapy.Spider):
    name = "product_spider"

    def start_requests(self):
        # connection = pymongo.MongoClient(
        #     settings['MONGODB_SERVER'],
        #     settings['MONGODB_PORT']
        # )
        # db = connection[settings['MONGODB_DB']]["products_pages_lists"]
        #store_pages = db.find_one({"_id": "chewy.com_dry_dog_food"})["links"]
        #urls = []
        #for item in store_pages:
        #    for link in item["links"]:
        #        urls.append(link)

        urls = [
            'https://www.chewy.com/purina-pro-plan-focus-adult-sensitive/dp/128666',
            # "https://www.bestbuy.com/site/dell-inspiron-2-in-1-15-6-touch-screen-laptop-intel-core-i5-8gb-memory-2tb-hard-drive-era-gray/6083543.p?skuId=6083543"
            # "https://www.petsmart.com/dog/food/dry-food/blue-wilderness-adult-dog-food---grain-free-natural-chicken-5149891.html",
            # "https://www.amazon.com/dp/B017Z51GCI/ref=strm_lighting_nad_1_3",
            # "https://www.amazon.com/1MORE-Headphones-Earphones-Compatible-Microphone/dp/B06XVVWWF4/ref=lp_12097478011_1_5?s=aht&ie=UTF8&qid=1513856650&sr=1-5&th=1",
            # "https://www.petco.com/shop/en/petcostore/product/dog/dog-food/merrick-grain-free-real-texas-beef-and-sweet-potato-dog-food",
            # "https://www.walmart.com/ip/Pedigree-Adult-Grilled-Steak-and-Vegetable-Flavor-Dry-Dog-Food-50-Pounds/49585273",
            # "https://www.pet-supermarket.co.uk/Canine-Choice-Adult-Medium-Dog-Food/p/I9287280",
            # "https://www.petsupplies.com/item/orijen-original-dry-dog-food/P00098/",
            # "https://www.worldforpets.com.au/products/70344",
            # "http://www.drsfostersmith.com/product/prod_display.cfm?pcatid=27586",
            # "https://jet.com/product/Blue-Buffalo-Wilderness-Grain-Free-Healthy-Weight-Chicken-Recipe-Dry-Dog-Food-24/8d946e567e2e480b9e5558e02f223f00",
            # "https://petstuff.com/blue-buffalo-wilderness-adult-small-breed-chicken-recipe-grain-free-dry-dog-food/dp/4188",
            # "http://www.dogfooddirect.com/store/p/827-NutriSource-Adult-Dog-Food.aspx",
            # "https://www.gofromm.com/fromm-four-star-nutritionals-beef-frittata-veg-food-for-dogs",
            # "https://www.petflow.com/product/acana/acana-singles-limited-ingredient-diet-duck-and-pear-formula-dry-dog-food",

            # "https://www.sweetwater.com/store/detail/214ceDlx",
            # "https://reverb.com/item/9682194-garrison-g-40-2006-aged-natural-gloss",
            # "https://www.musicartestore.com/en/sigma-jrc1ste-natural_0123675.html,"
            # "https://www.dawsons.co.uk/yamaha-f310-acoustic-guitar-natural",

            # "https://www.bestbuy.com/site/logitech-mx-anywhere-2s-wireless-laser-mouse-graphite/5870714.p?skuId=5870714"
            # "http://www.crucial.com/usa/en/ct250mx500ssd1",
            # "http://www.microcenter.com/product/476093/Blue_500_GB_NAND_SATA_30_60_GB-s_25_Internal_SSD"
            # "https://www.skullcandy.com/headphones/wireless-headphones/HESH3BT.html?cgid=1000&color=Red&skdy_mic=Universal",

            # "https://www.urbanears.com/ue_us_en/hellas"
            # "https://intl.target.com/p/lg-174-60-4k-ultra-hd-smart-led-tv/-/A-52702342"
        ]

        for url in urls:
            yield SplashRequest(
                url,
                self.parse,
                endpoint='execute',
                args={
                    'lua_source': script,
                    'timeout': 1800,
                    # 'proxy': 'tor',
                },
            )

    def parse(self, response):
        url = response.url
        html = response.body
        # product_model_name = "dry dog food"
        model_id = "dry_dog_food"

        product_page_src = ProductPageSrc()
        product = {}
        # ProductObject()
        # product_model = self.get_product_model(product_model_name)
        product_model = pro_model.load_model(model_id)

        store = data_utils.get_baseurl(url)
        product["_id"] = store + "_" + data_utils.get_page_path_name(url)
        product["product_model_id"] = model_id
        product["url"] = url
        product["store"] = store

        product_page_src["_id"] = (
            "src_" + store + "_" +
            data_utils.get_page_path_name(url)
        )
        product_page_src["product_id"] = (
            store + "_" +
            data_utils.get_page_path_name(url)
        )

        #
        # html process
        #

        # used by splash call to make sure the js/css files are loaded.
        baseurl = data_utils.get_full_baseurl(url)

        html_to_process = html.decode('utf-8')
        html_to_process = re.sub(r'[\n\r\t\\\n\\\t]*', '', html_to_process)
        # (
        #     html.decode('utf-8').re.sub(r'[\n\r\t\\\n\\\t]*', '', s)
        #     # .replace("\\n", '')
        #     # .replace("\\t", '')
        #     # .replace("\n", '')
        #     # .replace("\t", '')
        # )

        variation_keys = pro_model.get_variations_keys(product_model)

        # product page process object
        soup = product_page.run(html_to_process, variation_keys)

        # handle processed soup html

        product_page_src["html_src"] = html_to_process

        yield product_page_src

        #
        # export direct product info
        #

        product_title = product_page.get_product_title(soup)
        product_price = product_page.get_product_price(soup)
        product_image = product_page.get_product_image_src(soup)

        # product rating
        rating = product_page.get_product_rating(soup)
        # product info
        info = product_page.get_product_info(soup)
        # on page variation
        on_page_variation = product_page.get_on_page_variation(soup)

        product["title"] = product_title
        product["price"] = product_price
        product["rating"] = rating
        product["info"] = info
        product["variations"] = (
            [on_page_variation] if len(on_page_variation.keys()) > 0 else []
        )
        product["image_urls"] = [product_image] if product_image else []

        #
        # handle clickable product variation
        #
        xpath_title = product_page.get_title_xpath(soup)
        xpath_price = product_page.get_price_xpath(soup)

        # variation by clickable element
        clickable_items = product_page.get_clickable_variations_list(soup)

        yield product

        variation_xpath_keys = product_model["variationsXpathKeys"]

        for key in clickable_items:

            if key in variation_xpath_keys.keys():
                is_valid = bool(variation_xpath_keys[key]["is_valid"])
                if is_valid is False:
                    print("** variation_xpath_keys skip **")
                    continue

            request = scrapy.Request(url, self.parse_product_variation, meta={
                'splash': {
                    'endpoint': 'execute',
                    'args': {
                        'timeout': 1800,
                        "main_title_xpath": xpath_title,
                        "product_price_xpath": xpath_price,
                        "product_variation_holder_xpath": key,
                        "product_variation": clickable_items[key],
                        "baseurl": baseurl,
                        'lua_source': product_variation_script,
                        # 'proxy': 'tor',
                    },
                }
            })
            request.meta["product_variation_key"] = key
            request.meta["product"] = product
            request.meta["product_model"] = product_model

            yield request

    def parse_product_variation(self, response):

        product = response.meta["product"]
        product_model = response.meta["product_model"]

        xpath_key = response.meta["product_variation_key"]
        print("** this variation_xpath_keys **")
        print(xpath_key)
        #
        # check result if variation
        #
        data = json.loads(response.body.decode('utf8').replace("'", '"'))
        # check if variation results is valid
        variation_is_valid = pro_model.variation_is_valid(data["results"])
        print('** data["results"] **')
        print(data["results"])

        variation_xpath_keys = product_model["variationsXpathKeys"]
        if(variation_is_valid is False and xpath_key not in variation_xpath_keys.keys()):
            pro_model.update_model_vari_xpath_keys(
             product_model,
             xpath_key,
             product["store"],
             variation_is_valid
            )

        if(variation_is_valid):
            # update xpath keys
            if(xpath_key not in variation_xpath_keys.keys()):
                pro_model.update_model_vari_xpath_keys(
                 product_model,
                 xpath_key,
                 product["store"],
                 variation_is_valid
                )
            # normalize
            variation_results = (
                pro_model.variation_normalize(data["results"], product_model)
            )

            # export product info
            product["variations"].append(variation_results)

            # export product model with new data
            pro_model.update_model_vari_obj(product_model, variation_results)

            yield product
