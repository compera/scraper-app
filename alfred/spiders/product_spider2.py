"""Product page scraper."""
import scrapy
import pymongo
import datetime
import json
import re
import random
from scrapy.conf import settings
from scrapy.utils.response import get_base_url
from scrapy_splash import SplashRequest
from alfred.items import (
    ProductBasicInfo,
    ProductPeriodicInfo,
    ProductPeriodicVariations,
    ModelXpathKeys,
)

from data_utils import data_utils
from html_process.product_page import product_page

from data_utils import (
    product_utils,
    model_utils
)

script = """
    -- main script
    function main(splash)
      local addPosition = splash:jsfunc([[
        function() {
          var allNodes = document.querySelectorAll("*")

          for (let i=0; i < allNodes.length - 1; i++) {
              try {
                allNodes[i].setAttribute("pos", JSON.stringify(
                    allNodes[i].getBoundingClientRect()));
                let border = {
                    "px": (
                      window.getComputedStyle(allNodes[i], null)
                      .getPropertyValue("border-top-width")
                    ),
                    "color": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("border-bottom-color")
                    ),
                    "style": (
                      window.getComputedStyle(allNodes[i], null)
                      .getPropertyValue("border-right-style")
                    )
                }

                let al_style = {
                    "color": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("color")
                    ),
                    "font-size": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("font-size")
                    ),
                    "text-decoration": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("text-decoration")
                    ),
                    "font-weight": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("font-weight")
                    ),
                    "cursor": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("cursor")
                    ),
                    "background-color": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("background-color")
                    ),
                    "border": border,
                }
                allNodes[i].setAttribute("pos", JSON.stringify(
                    allNodes[i].getBoundingClientRect()));
                allNodes[i].setAttribute("al_style", JSON.stringify(al_style));
              }
              catch (e) {
                 pass;
              }
          }
        }
      ]])

      assert(splash:go(splash.args.url))
      assert(splash:wait(1.5))

      splash:set_viewport_size(1980, 8020)

      addPosition()

      local html = splash:html()
      return html
    end
    """

variations_script = """
    -- main script
    function main(splash)

      local addPosition = splash:jsfunc([[
        function() {
          var allNodes = document.querySelectorAll("*")

          for (let i=0; i < allNodes.length - 1; i++) {
              try {
                allNodes[i].setAttribute("pos", JSON.stringify(
                    allNodes[i].getBoundingClientRect()));
                let border = {
                    "px": (
                      window.getComputedStyle(allNodes[i], null)
                      .getPropertyValue("border-top-width")
                    ),
                    "color": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("border-bottom-color")
                    ),
                    "style": (
                      window.getComputedStyle(allNodes[i], null)
                      .getPropertyValue("border-right-style")
                    )
                }

                let al_style = {
                    "color": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("color")
                    ),
                    "font-size": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("font-size")
                    ),
                    "text-decoration": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("text-decoration")
                    ),
                    "font-weight": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("font-weight")
                    ),
                    "cursor": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("cursor")
                    ),
                    "background-color": (
                        window.getComputedStyle(allNodes[i], null)
                        .getPropertyValue("background-color")
                    ),
                    "border": border,
                }
                allNodes[i].setAttribute("pos", JSON.stringify(
                    allNodes[i].getBoundingClientRect()));
                allNodes[i].setAttribute("al_style", JSON.stringify(al_style));
              }
              catch (e) {
                 pass;
              }
          }
        }
      ]])

      local get_element_by_xpath = splash:jsfunc([[
        function(xpath) {
          var element = document.evaluate(
            xpath,
            document,
            null,
            XPathResult.ANY_TYPE,
            null
          )
          var element_node, element_nodes = []
          while (element_node = element.iterateNext()) {
            element_nodes.push(element_node);
          }
          return element_nodes[0] ? element_nodes[0] : 'undefined'
        }
      ]])

      local get_innerText = splash:jsfunc([[
        function(element) {
          return (
            element.innerText ? element.innerText : 'undefined'
          )
        }
      ]])

      local click_on_element = splash:jsfunc([[
        function(element) {
          let clickOn = element
          let excute = true
          if(element.children.length === 1) {
              clickOn = element.children[0]
          }
          clickOn.click()
        }
      ]])

      local click_on_element_children = splash:jsfunc([[
        function(element) {
          if (element.children.length > 0) {
            try {
              element.children[0].click()
            }
            catch(err) {
              console.log(err)
            }
          }
        }
      ]])

      local select_change = splash:jsfunc([[
        function(optionElem, selectElem) {
            try {
             optionElem.selected = true;
             const event = new Event('change', {bubbles: true});
             selectElem.dispatchEvent(event);
            }
            catch(err) {
              console.log(err)
            }
        }
      ]])

      local is_select_element = splash:jsfunc([[
        function(elment) {
          return elment.options ? true : false
        }
      ]])

      local select_option_value = splash:jsfunc([[
        function(optionElem) {
          return optionElem.value ? optionElem.value != "" : false
        }
      ]])

      local sel = splash:jsfunc([[
        function(optionElem) {
          return optionElem.value ? optionElem.value != "" : false
        }
      ]])

      assert(splash:go(splash.args.url, splash.args.baseurl))
      assert(splash:wait(1.5))

      splash:set_viewport_size(1980, 8020)

      local title_xpath = splash.args.title_xpath
      local price_xpath = splash.args.price_xpath
      local main_clickable_xpath = splash.args.main_clickable_xpath
      local clickable_xpaths = splash.args.clickable_xpaths

      local main_clickable_elem = get_element_by_xpath(
          splash.args.main_clickable_xpath
      )

      local is_select_elem = is_select_element(main_clickable_elem)

      local results = {}
      local htmls = {}
      for i, v in ipairs( clickable_xpaths ) do
        local variation_xpath = clickable_xpaths[i]
        local variation_element = get_element_by_xpath(variation_xpath)

        local variation_elem_exist = variation_element ~= 'undefined'

        local click_way_check = (
            variation_elem_exist and
            is_select_elem == false
        )

        local select_way_check = (
            variation_elem_exist and
            is_select_elem and
            select_option_value(variation_element)
        )

        if click_way_check or select_way_check then
          obj = {}
          if is_select_elem then
            select_change(variation_element, main_clickable_elem)
          else
            click_on_element(variation_element)
          end

          assert(splash:wait(6))

          addPosition()

          local title_element = get_element_by_xpath(title_xpath)
          local price_element = get_element_by_xpath(price_xpath)
          local title = get_innerText(title_element)
          local price = get_innerText(price_element)
          local variation = get_innerText(
              get_element_by_xpath(variation_xpath)
          )

          obj["title"] = title
          obj["price"] = price
          obj["variation"] = variation

          htmls[#htmls+1] = splash:html()
          results[#results+1] = obj
        end
      end

      return {
        htmls=htmls,
        results=results
      }
    end
    """


class ProductSpider(scrapy.Spider):
    name = "product_spider2"

    def start_requests(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]["products_pages_lists"]
        store_pages = db.find_one({"_id": "petsmart.com_dry_dog_food"})["links"]
        urls = []
        for item in store_pages:
           for link in item["links"]:
               urls.append(link)

        # urls = [
        #     # 'https://www.chewy.com/purina-pro-plan-focus-adult-sensitive/dp/128666',
        #     # "https://www.bestbuy.com/site/dell-inspiron-2-in-1-15-6-touch-screen-laptop-intel-core-i5-8gb-memory-2tb-hard-drive-era-gray/6083543.p?skuId=6083543"
        #     # "https://www.petsmart.com/dog/food/dry-food/blue-wilderness-adult-dog-food---grain-free-natural-chicken-5149891.html",
        #     # "https://www.amazon.com/dp/B017Z51GCI/ref=strm_lighting_nad_1_3",
        #     # "https://www.amazon.com/1MORE-Headphones-Earphones-Compatible-Microphone/dp/B06XVVWWF4/ref=lp_12097478011_1_5?s=aht&ie=UTF8&qid=1513856650&sr=1-5&th=1",
        #     # "https://www.petco.com/shop/en/petcostore/product/dog/dog-food/merrick-grain-free-real-texas-beef-and-sweet-potato-dog-food",
        #     # "https://www.walmart.com/ip/Pedigree-Adult-Grilled-Steak-and-Vegetable-Flavor-Dry-Dog-Food-50-Pounds/49585273",
        #     # "https://www.pet-supermarket.co.uk/Canine-Choice-Adult-Medium-Dog-Food/p/I9287280",
        #     # "https://www.petsupplies.com/item/orijen-original-dry-dog-food/P00098/",
        #     # "https://www.worldforpets.com.au/products/70344",
        #     # "http://www.drsfostersmith.com/product/prod_display.cfm?pcatid=27586",
        #     "https://jet.com/product/Blue-Buffalo-Wilderness-Grain-Free-Healthy-Weight-Chicken-Recipe-Dry-Dog-Food-24/8d946e567e2e480b9e5558e02f223f00",
        #     # "https://petstuff.com/blue-buffalo-wilderness-adult-small-breed-chicken-recipe-grain-free-dry-dog-food/dp/4188",
        #     # "http://www.dogfooddirect.com/store/p/827-NutriSource-Adult-Dog-Food.aspx",
        #     # "https://www.gofromm.com/fromm-four-star-nutritionals-beef-frittata-veg-food-for-dogs",
        #     # "https://www.petflow.com/product/acana/acana-singles-limited-ingredient-diet-duck-and-pear-formula-dry-dog-food",

        #     # "https://www.sweetwater.com/store/detail/214ceDlx",
        #     # "https://reverb.com/item/9682194-garrison-g-40-2006-aged-natural-gloss",
        #     # "https://www.musicartestore.com/en/sigma-jrc1ste-natural_0123675.html,"
        #     # "https://www.dawsons.co.uk/yamaha-f310-acoustic-guitar-natural",

        #     # "https://www.bestbuy.com/site/logitech-mx-anywhere-2s-wireless-laser-mouse-graphite/5870714.p?skuId=5870714"
        #     # "http://www.crucial.com/usa/en/ct250mx500ssd1",
        #     # "http://www.microcenter.com/product/476093/Blue_500_GB_NAND_SATA_30_60_GB-s_25_Internal_SSD"
        #     # "https://www.skullcandy.com/headphones/wireless-headphones/HESH3BT.html?cgid=1000&color=Red&skdy_mic=Universal",

        #     # "https://www.urbanears.com/ue_us_en/hellas"
        #     # "https://intl.target.com/p/lg-174-60-4k-ultra-hd-smart-led-tv/-/A-52702342"
        # ]

        for url in urls:
            yield scrapy.Request(
                url,
                self.parse,
                meta={
                    'splash': {
                        'endpoint': 'execute',
                        'args': {
                            'lua_source': script,
                            'timeout': 1800,
                            # 'proxy': 'tor',
                        },
                    }
                }
            )


    def parse(self, response):
        model_id = "dry_dog_food"
        model = model_utils.get_model(model_id)

        url = response.url
        html = response.body

        product_store = data_utils.get_baseurl(url)
        product_id = (
            product_store + data_utils.get_page_path_name(url)
        )
        time_key = datetime.datetime.now().strftime("%d-%m-%y-%H-%M")

        soup = self.html_process(html.decode('utf-8'), model)

        # BASIC INFO
        if (product_utils.get_product(product_id) is None):
            product_basic_info = ProductBasicInfo(
                self.create_basic_info(
                    url,
                    soup,
                    product_store,
                    product_id,
                    model
                )
            )
            yield product_basic_info

        # debug
        data_utils.export_product_page_src_as_html(url, soup.prettify())

        # PERIODIC INFO
        periodic_info = ProductPeriodicInfo(
            self.create_periodic_info(soup, product_id, time_key)
        )
        yield periodic_info

        # ON PAGE VARIATIONS
        on_page_variations = (
            self.create_on_page_variations(
                soup,
                model,
                product_id,
                time_key
            )
        )
        if on_page_variations is not None:
            yield ProductPeriodicVariations(on_page_variations)

        # CLICKABLE VARIATIONS

        # used by splash call to make sure the js/css files are loaded.
        baseurl = data_utils.get_full_baseurl(url)
        clickable_items_xpaths = (
            product_page.get_clickable_variations_list(soup)
        )

        model_xpath_keys = model["variationsXpathKeys"]
        for main_key in clickable_items_xpaths:
            if main_key in model_xpath_keys.keys():
                valid = bool(model_xpath_keys[main_key]["valid"])
                if valid is False:
                    print("** variation xpath keys skip **")
                    continue

            request = scrapy.Request(
                url,
                self.parse_variation_page,
                meta={
                    'splash': {
                        'endpoint': 'execute',
                        'args': {
                            'timeout': 3600,
                            "baseurl": baseurl,
                            "title_xpath": (
                                product_page.get_title_xpath(soup)
                            ),
                            "price_xpath": (
                                product_page.get_price_xpath(soup)
                            ),
                            "main_clickable_xpath": main_key,
                            "clickable_xpaths": (
                                clickable_items_xpaths[main_key]
                            ),
                            'lua_source': variations_script,
                            # 'proxy': 'tor',
                        },
                    }
                }
            )
            request.meta["main_xpath_key"] = main_key
            request.meta["model"] = model
            request.meta["product_id"] = product_id
            request.meta["product_store"] = product_store
            request.meta["time_key"] = time_key

            yield request

    def parse_variation_page(self, response):
        url = response.url
  
        main_xpath_key = response.meta["main_xpath_key"]
        model = response.meta["model"]
        product_id = response.meta["product_id"]
        product_store = response.meta["product_store"]
        time_key = response.meta["time_key"]

        data = json.loads(response.body)
        # check if variation results is valid
        variation_is_valid = (
            model_utils.check_if_valid(data["results"])
        )

        if main_xpath_key not in model["variationsXpathKeys"].keys():
            yield ModelXpathKeys({
                "_model_id": model["_id"],
                "main_xpath_key": main_xpath_key,
                "valid": variation_is_valid,
                "store": product_store
            })

        variation_should_be_valid = (
            main_xpath_key in model["variationsXpathKeys"].keys() and
            model["variationsXpathKeys"][main_xpath_key]["valid"]
        )

        if variation_is_valid or variation_should_be_valid:
            # debug
            for vari_index in data["htmls"]:
                html = data["htmls"][vari_index]
                data_utils.export_product_page_src_as_html(
                    url,
                    html,
                    vari_index
                )
            updated_resutls = self.process_variations_html_src(
                data["results"],
                data["htmls"],
                model
            )
            clickable_periodic_variations = ProductPeriodicVariations(
                self.create_clickable_variations(
                    updated_resutls,
                    model,
                    product_id,
                    time_key
                )
            )
            yield clickable_periodic_variations

    def html_process(self, html, model):
        # bs html process
        html_to_process = html
        html_to_process = re.sub(r'[\n\r\t\\\n\\\t]*', '', html_to_process)
        variation_types = (
            model_utils.get_all_variations_types(model)
        )

        return product_page.run(html_to_process, variation_types)

    def create_basic_info(self, url, soup, _store, _id, _model):
        product_basic_info = {}

        product_image = product_page.get_product_image_src(soup)

        product_basic_info["_id"] = _id
        product_basic_info["store"] = _store
        product_basic_info["product_model_id"] = _model["_id"]
        product_basic_info["url"] = url
        product_basic_info["image_urls"] = (
            [product_image] if product_image else []
        )

        return product_basic_info

    def create_periodic_info(self, soup, _id, _time_key):
        periodic_info = {}

        periodic_info["title"] = product_page.get_product_title(soup)
        periodic_info["price"] = product_page.get_product_price(soup)
        periodic_info["rating"] = product_page.get_product_rating(soup)

        periodic_info["_product_id"] = _id
        periodic_info["_time_key"] = _time_key

        return periodic_info

    def create_on_page_variations(self, soup, model, prod_id, time_key):
        res = product_page.get_on_page_variation(soup)

        if len(res.keys()) == 0:
            return None

        vari = {}

        vari["model_vari_id"] = (
            model_utils.get_variation_id(model, res["type"])
        )
        vari["list"] = res["list"]

        vari["_product_id"] = prod_id
        vari["_time_key"] = time_key

        return vari

    def create_clickable_variations(self, results, model, prod_id, time_key):
        vari = {}

        vari["list"] = []
        temp_variation_list = []

        for index in results:
            vari_type_norm = (
                results[index]["variation"]
                .replace("-", " ")
                .replace("(", "")
                .replace(")", "")
            ).strip()
            results[index]["variation"] = vari_type_norm.lower()
            temp_variation_list.append(vari_type_norm)

        variation_id = model_utils.get_variation_id(model, temp_variation_list)
        vari["model_vari_id"] = variation_id
        model_variation = model["variations"][variation_id]
        for index in results:
            variation_norm2 = results[index]["variation"]
            for syn in model_variation["synonyms"]:
                if syn in results[index]["variation"]:
                    variation_norm2 = (
                        results[index]["variation"].replace(
                            syn,
                            model_variation["name"]
                        )
                    )

            vari["list"].append({
                'price': results[index]["price"],
                'title': results[index]["title"],
                'variation': variation_norm2
            })

        vari["_product_id"] = prod_id
        vari["_time_key"] = time_key

        return vari

    def create_model_variations(self, model, product_variations):
        model_variation = {}
        model_variation_id = product_variations["model_vari_id"]

        new_varis_list = []
        for vari_obj in product_variations["list"]:
            variation = vari_obj["variation"]
            if variation != 'undefined':
                new_varis_list.append(variation)

        add_prev_vari_list = (
            len(model["variations"].keys()) and
            model["variations"][model_variation_id]
        )
        if add_prev_vari_list:
            new_varis_list = list(set(
                new_varis_list +
                model["variations"][model_variation_id]["vari_list"]
            ))

        model_variation["_model_id"] = model["_id"]
        model_variation["_model_vari_id"] = model_variation_id
        model_variation["valid"] = True
        model_variation["vari_list"] = new_varis_list
        model_variation["type"] = (
            model_utils.generate_variation_type(new_varis_list)
        )
        return model_variation

    def process_variations_html_src(self, results, htmls, model):
        updated_results = results
        for vari_index in results:
            vari = results[vari_index]
            process_check = (
                vari["price"] == 'undefined' or
                vari["variation"] == 'undefined'
            )
            if process_check:
                html = htmls[vari_index]
                soup = self.html_process(html, model)
                # PRICE
                updated_results[vari_index]["price"] = (
                    product_page.get_product_price(soup)
                )

        return updated_results
