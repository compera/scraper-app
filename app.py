"""Import BeautifulSoup."""
# from bs4 import BeautifulSoup
import pymongo
from html_process.common import data_loader
from html_process.page import page
from html_process.product_page.semantic_role import semantic_role
from html_process.product_page.semantic_role import (
    product_title,
    product_price,
    product_image,
    main_container,
)
from html_process.product_page.semantic_role.product_variations import (
    product_variations
)
from html_process.page.semantic_type.leaf_semantic_type import (
    leaf_semantic_type
)
from html_process.product_page.semantic_role.product_rating import (
    product_rating
)
from html_process.product_page.semantic_role.product_info import (
    product_info
)

MONGODB_SERVER = "localhost"
MONGODB_PORT = 27017
MONGODB_DB = "compera"


# data_files = data_loader.get_files()
# './data/bestbuy.com/_site_dell-inspiron-2-in-1-15-6-touch-screen-laptop-intel-core-i5-8gb-memory-2tb-hard-drive-era-gray_6083543.p/src.json'
# './data/chewy.com/_purina-pro-plan-focus-adult-sensitive_dp_128666/src.json'
# './data/petsmart.com/_dog_food_dry-food_blue-wilderness-adult-dog-food---grain-free-natural-chicken-5149891.html/src.json'

# './data/petstuff.com/_blue-buffalo-wilderness-adult-small-breed-chicken-recipe-grain-free-dry-dog-food_dp_4188/src.json'

# './data/worldforpets.com.au/_products_70344/src.json'
# './data/gofromm.com/_fromm-four-star-nutritionals-beef-frittata-veg-food-for-dogs/src.json'
# './data/walmart.com/_ip_Pedigree-Adult-Grilled-Steak-and-Vegetable-Flavor-Dry-Dog-Food-50-Pounds_49585273/src.json'
# './data/pet-supermarket.co.uk/_Canine-Choice-Adult-Medium-Dog-Food_p_I9287280/src.json'
# './data/petsupplies.com/_item_orijen-original-dry-dog-food_P00098_/src.json'

# './data/drsfostersmith.com/_product_prod_display.cfm/src.json'

# './data/dawsons.co.uk/_yamaha-f310-acoustic-guitar-natural/src.json'
# './data/sweetwater.com/_store_detail_214ceDlx/src.json'
# './data/bestbuy.com/_site_logitech-mx-anywhere-2s-wireless-laser-mouse-graphite_5870714.p/src.json'

# data = data_loader.get_html_data(
#     './data/bestbuy.com/_site_logitech-mx-anywhere-2s-wireless-laser-mouse-graphite_5870714.p/src.json'
# )
connection = pymongo.MongoClient(
    MONGODB_SERVER,
    MONGODB_PORT
)

db = connection[MONGODB_DB]
collection = db["product_page_src"]

index_html1 = (
    collection.find_one(
        {
            "_id": "src_petco.com__shop_en_petcostore_product_dog_dog-food_merrick-grain-free-real-texas-beef-and-sweet-potato-dog-food"
        }
    )
)["html_src"]


# index_soup1 = BeautifulSoup(index_html1, "lxml")

soup = page.run(index_html1)
# html_prep.clean_html(index_soup1)
# html_prep.fix_untagged_string(index_soup1, index_soup1)
# leaf_process.walker(index_soup1.body)
# leaf_chain_process.process(index_soup1.body)
# holder_process.walker(index_soup1.body)
# holder_chain_process.process(index_soup1.body)

product_title.find(
    soup,
    leaf_semantic_type.get_all_titles(soup),
    soup.title.string.strip()
)
product_price.find(soup, leaf_semantic_type.get_all_prices(soup))
product_image.find(soup)
main_container.find(soup)
product_variations.find(soup, ["lb"])
product_rating.find(soup)
product_info.find(soup)


with open('render1.html', 'w', encoding="utf-8") as outfile:
    outfile.write(soup.prettify())
