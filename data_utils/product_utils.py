import pymongo
from scrapy.conf import settings
import json


def get_product(product_id):
    """Get product object if exist, otherwise return None."""
    connection = pymongo.MongoClient(
        settings['MONGODB_SERVER'],
        settings['MONGODB_PORT']
    )

    db = connection[settings['MONGODB_DB']]
    products_collection = db["products"]
    product = products_collection.find_one({"_id": product_id})
    connection.close()

    return product
