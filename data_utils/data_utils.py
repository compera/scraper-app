"""Data utils."""
from pathlib import Path
import json
from urllib.parse import urlsplit, urlunsplit

MAIN_OUTPUT_DIR = "debug"
PRODUCT_OUTPUT_DIR = "product_pages"
PRODUCT_LIST_OUTPUT_DIR = "product_list_pages"


def get_baseurl(url):
    """Get root domain name. string for indexes."""
    return "{0.netloc}".format(urlsplit(url)).replace("www.", "")


def get_full_baseurl(url):
    """Get root domain name. using in splash requestes to load resources."""
    split_url = urlsplit(url)
    return urlunsplit((split_url.scheme, split_url.netloc, "", "", ""))


def get_page_path_name(url):
    """Get page path from url. string for indexes."""
    return "{0.path}".format(urlsplit(url)).replace("/", "_")


def _get_data_dir(page_url):
    """Return website data path module, create if not exists."""
    domain_path = get_baseurl(page_url)
    data_dir = Path('./', MAIN_OUTPUT_DIR, domain_path)
    data_dir.mkdir(exist_ok=True, parents=True)
    return data_dir

#
# PROUDCT PAGE
#


# def export_product_page_src(page_dir, page_url, page_label, html_src):
#     """
#     Get root domain name.

#     Params:
#     page_dir - Path module point to current page dir.
#     page_url - Full url.
#     page_label - product label.
#     html_src - html code.
#     """
#     src_filename = page_dir.joinpath('src.json')

#     src_object = _create_product_src_model(page_url, page_label, html_src)
#     with src_filename.open("w") as f:
#         f.write(json.dumps(src_object, indent=4))



def export_product_page_src_as_html(page_url, html_code, file_name='index'):
    """Create index html file from src."""
    page_dir = _get_page_dir(page_url)
    html_filename = page_dir.joinpath(str(file_name) + '.html')
    with html_filename.open("w", encoding="utf-8") as f:
        f.write(html_code)


def _get_page_dir(page_url):
    """Return website data path module point to page folder."""
    data_dir = _get_data_dir(page_url)
    page_path = get_page_path_name(page_url)
    page_dir = data_dir.joinpath(PRODUCT_OUTPUT_DIR, page_path)
    page_dir.mkdir(exist_ok=True, parents=True)
    return page_dir

def _create_product_src_model(page_url, page_label, html_src=""):
    """Create src object to export."""
    src_object = {}
    src_object["page_url"] = str(page_url)
    src_object["page_label"] = page_label
    src_object["html_src"] = html_src.decode("utf-8")

    return src_object


# def _product_info_object(title, price, rating, info, variation):
#     product_info = {}
#     product_info["main"] = {
#         "title": title,
#         "price": price,
#         "rating": rating,
#     }
#     product_info["info"] = info
#     product_info["variation"] = variation

#     return product_info


# def export_product_info(page_dir, title, price, rating, info, variation):
#     """Create index html file from src."""
#     filename = page_dir.joinpath('product_info.json')
#     product_info = _product_info_object(title, price, rating, info, variation)

#     with filename.open("w") as f:
#         f.write(json.dumps(product_info, indent=4))


# def add_variations_to_product_info(page_dir, variation_key, data):
#     """Return product info file."""
#     filename = page_dir.joinpath('product_info.json')
#     product_info = {}

#     if(filename.exists()):
#         product_info = json.loads(filename.read_text())

#     product_info["variation"][variation_key] = data
#     with filename.open("w") as f:
#         f.write(json.dumps(product_info, indent=4))


# def add_variations_to_product_info2(current_variation, variation_key, data):
#     """Return product info file."""
#     product_info = current_variation
#     product_info[variation_key] = data

#     return product_info



#
# PROUDCT LIST
#


# def get_list_page_main_dir(data_dir, list_product_type):
#     """Return website data path module point to page list folder."""
#     list_type = '_'.join(list_product_type.split(" "))
#     list_page_main_dir = data_dir.joinpath(
#         PRODUCT_LIST_OUTPUT_DIR,
#         list_type
#     )
#     list_page_main_dir.mkdir(exist_ok=True, parents=True)
#     return list_page_main_dir


# def get_list_page_dir(list_page_main_dir, page_url):
#     """Return website data path module point to page list folder."""
#     list_page_path = get_page_path_name(page_url)
#     list_page_dir = list_page_main_dir.joinpath(list_page_path)
#     list_page_dir.mkdir(exist_ok=True, parents=True)
#     return list_page_dir


# def list_info_data(list_main_dir, prod_type, total_pagination_pages):
#     """Generate info list data. holding info about the list."""
#     filename = list_main_dir.joinpath("a_info.json")
#     info_obj = {}
#     info_obj["list_product_type"] = prod_type
#     info_obj["total_pagination_pages"] = total_pagination_pages
#     with filename.open("w") as f:
#         f.write(json.dumps(info_obj, indent=4))
