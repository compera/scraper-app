"""import Path."""
# from pathlib import Path
import pymongo
from scrapy.conf import settings
import json
from collections import Counter
import re


def get_model(model_id):
    """Load product model from db.

    Keyword arguments:
    model_id -- the model id
    """
    connection = pymongo.MongoClient(
        settings['MONGODB_SERVER'],
        settings['MONGODB_PORT']
    )

    db = connection[settings['MONGODB_DB']]
    model_collection = db["product_models"]
    model = model_collection.find_one({"_id": model_id})

    connection.close()

    return model


def get_all_variations_types(model):
    "Return list of variations types from model."
    varis_types = []
    for vari_key in model["variations"]:
        vari_obj = model["variations"][vari_key]
        varis_types += vari_obj["on_page_usage"]

    return varis_types


def get_variation_id(model, variation):
    "Return vari id of the variation type in product model."
    variation_id_key = ""
    this_variation = variation
    if type(variation) is list:
        this_vari_counter = (
            _get_variation_type_counter(variation)
        )
        this_variation = _get_variation_type(this_vari_counter)

    this_variation = this_variation.lower()

    for key in model["variations"]:
        model_vari_obj = model["variations"][key]
        model_syn = (
            model_vari_obj["synonyms"] + model_vari_obj["on_page_usage"]
        )
        for syn in model_syn:
            if this_variation == syn:
                variation_id_key = key
                continue

    return str(variation_id_key)


def check_if_valid(results):
    """Check if variation object is valid.

    Keyword arguments:
    results -- the variation object
    """
    price_variation_counter = len(list(results.keys()))
    price_holder = []

    for key in results:
        price_holder.append(results[key]["price"])

    price_variation_set = set(price_holder)
    return (
        True if (
            len(price_holder) > 1 and
            price_variation_counter > 0 and
            price_variation_counter - len(price_variation_set) < 1
        )
        else False
    )


def generate_variation_type(vars_list):
    """Get variation type warpper."""
    counter = _get_variation_type_counter(vars_list)
    return _get_variation_type(counter)


def _get_variation_type(words_counter):
    most_common = words_counter.most_common(1)[0][1]
    variation_words_list = []
    for word_c in words_counter.items():
        if word_c[1] == most_common:
            variation_words_list.append(word_c[0])

    return " ".join(variation_words_list)


def _get_variation_type_counter(vars_list):
    """Extract repeated variation type(word) from the variation types list.

    Keyword arguments:
    vars_list -- list of variations types
    """
    var_list_joind = ' '.join(vars_list)
    reg = re.compile('\S{1,}')
    # words_counter = Counter(ma.group() for ma in reg.finditer(var_list_joind))
    # most_common = words_counter.most_common(1)[0][1]
    # variation_words_list = []
    # for word_c in words_counter.items():
    #     if word_c[1] == most_common:
    #         variation_words_list.append(word_c[0])

    # return " ".join(variation_words_list)
    return Counter(ma.group() for ma in reg.finditer(var_list_joind))
