"""Product list testing."""
from html_process.product_list_page import product_list_page
from html_process.common import data_loader

# './data/chewy.com/product_list_pages/dry_dog_food/_b_dry-food-294/src.json'
# './data/petsmart.com/product_list_pages/dry_dog_food/_dog_food_dry-food_/src.json'
# './data/petsupplies.com/product_list_pages/dry_dog_food/_dog-supplies_food_dry-dog-food_9359_/src.json'
# './data/drsfostersmith.com/product_list_pages/dry_dog_food/_dog-supplies_food_dry-food_ps_c_3307_28497_28498src.json'


data = data_loader.get_html_data(
    './data/petsupplies.com/product_list_pages/dry_dog_food/_dog-supplies_food_dry-dog-food_9359_/src.json'
)
index_html = data["src"]
soup = product_list_page.run(index_html)

info = product_list_page.pagination_process_info(soup)

print("interval_start_from: ", str(info["interval_start_from"]))
print("total_pages: ", str(info["total_pages"]))
print("total_interval: ", str(info["total_interval"]))

print("baseurl: ", str(info["baseurl"]))
print("page_interval: ", str(info["page_interval"]))

pagination_interval = range(
    info["interval_start_from"],
    info["total_interval"] + info["page_interval"],
    info["page_interval"]
)
# pages_index = enumerate(pagination_interval, 2)

# for idx, interval in pages_index:
#     print("index: ", str(idx))
#     print("interval: ", str(interval))

with open('render1.html', 'w', encoding="utf-8") as outfile:
    outfile.write(soup.prettify())
