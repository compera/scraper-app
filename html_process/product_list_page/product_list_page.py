"""Run all process for product page."""
from html_process.common import utils
from html_process.page import page
from html_process.product_list_page.semantic_role import semantic_role
from html_process.product_list_page.semantic_role.lib import (
    product_box,
    product_box_container,
    list_pagination,
)
from html_process.common import (
    selector,
)


def run(html_src):
    """Process html src code. return processed soup obj."""
    soup = page.run(html_src)
    product_box.find(soup)
    product_box_container.find(soup)
    list_pagination.find(soup)

    return soup


def export_to_html(soup):
    """Return html src code."""
    return soup.prettify()


def get_boxes_container_xpath(soup):
    """Return xpath."""
    container_tag = semantic_role.get_boxes_container(soup)
    return selector.xpath(container_tag)


def _all_pagination_items(soup):
    """Warper for semantic role method, return all pagination items."""
    container = semantic_role.get_pagination_container(soup)
    return semantic_role.get_all_pagination_items(container)


def _extract_interval_from_href(href, diff_index):
    href_clean = href[diff_index + 1:]
    remove_from_index = 0

    for c in href_clean:
        if(utils.is_number(c)):
            remove_from_index += 1
        else:
            break

    return href_clean[:remove_from_index]


def _total_pages_index(last_item):
    return last_item.string.strip()


def _get_pagination_last_item(all_items):
    for item in reversed(all_items):
        if item.name is not None:
            if utils.is_number(item.string.strip()):
                return item


def _get_pagination_baseurl(href, diff_index):
    """Return string href pagination baseurl."""
    first_part = href[:diff_index + 1]
    second_part = href[diff_index + 1:]
    remove_from_index = 0
    for c in second_part:
        if(utils.is_number(c)):
            remove_from_index += 1
        else:
            break
    second_part = second_part[remove_from_index:]
    return first_part + "TO_REPLACE" + second_part


def _str_find_first_diff(a, b):
    """Return the first differnce index"""
    shorter = min(len(a), len(b))
    index = 0
    for i in range(shorter-1):
        a_sub = a[i:i+2]
        b_sub = b[i:i+2]
        if a_sub != b_sub:
            index = i
            break

    return int(index)


def _get_pagination_interval(href_one, href_two, diff_index):
    """Return interval of the pagination."""
    href_one_page = _extract_interval_from_href(href_one, diff_index)
    href_two_page = _extract_interval_from_href(href_two, diff_index)

    return abs(int(href_one_page) - int(href_two_page))


def pagination_process_info(soup):
    """Return pagination information."""
    all_items = _all_pagination_items(soup)
    href_two = all_items[1]["href"]
    href_three = all_items[2]["href"]
    diff_index = _str_find_first_diff(href_two, href_three)
    # interval
    interval = _get_pagination_interval(href_two, href_three, diff_index)

    # Start from
    interval_start_from = _extract_interval_from_href(href_two, diff_index)

    # baseurl
    baseurl = _get_pagination_baseurl(href_two, diff_index)

    last_item = _get_pagination_last_item(all_items)
    # Total pages
    total_pages = _total_pages_index(last_item)
    # Total interval
    total_interval = _extract_interval_from_href(last_item["href"], diff_index)

    return {
        "page_interval": int(interval),
        "interval_start_from": int(interval_start_from),
        "baseurl": baseurl,
        "total_pages": int(total_pages),
        "total_interval": int(total_interval),
    }
