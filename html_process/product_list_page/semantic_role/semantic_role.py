"""Semantic role list page."""
from html_process.common.app_var import (
    # SEMANTIC_ROLE,
    SEM_ROLE_CONTAINER_PRODUCT_BOXES,
    SEM_ROLE_PAGINATION,
    SEM_ROLE_PAGINATION_ITEM,
)


def get_boxes_container(soup):
    """Return product boxes container tag."""
    return soup.find(semantic_role=(SEM_ROLE_CONTAINER_PRODUCT_BOXES))


def get_pagination_container(soup):
    """Return pagination container tag."""
    return soup.find(semantic_role=(SEM_ROLE_PAGINATION))


def get_all_pagination_items(container):
    """Return list of all numbers pagination items."""
    return container.find_all(semantic_role=(SEM_ROLE_PAGINATION_ITEM))
