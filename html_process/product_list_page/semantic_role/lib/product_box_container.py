"""Product box container semantic role."""
from html_process.common.app_var import (
    SEMANTIC_ROLE,
    SEM_ROLE_PRODUCT_BOX,
    SEM_ROLE_CONTAINER_PRODUCT_BOXES,
)


def _walker_up(parent, number_boxes):
    """Start from first box tag and go up to find div contain all of them."""
    if(parent is not None and parent.name is not None):
        contain_boxes_num = (
            len(parent.find_all(semantic_role=SEM_ROLE_PRODUCT_BOX))
        )

        if(contain_boxes_num == number_boxes):
            parent[SEMANTIC_ROLE] = SEM_ROLE_CONTAINER_PRODUCT_BOXES
            parent["style"] = "border: black 3px solid !important"
        else:
            _walker_up(parent.parent, number_boxes)


def find(soup):
    """Find product box container role."""
    all_products_box = soup.find_all(semantic_role=SEM_ROLE_PRODUCT_BOX)
    first_box = all_products_box[0]
    number_of_boxes = len(all_products_box)

    _walker_up(first_box, number_of_boxes)
