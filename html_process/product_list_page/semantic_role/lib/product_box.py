"""Product box semantic role."""
from html_process.page.tag.tag_position import tag_position
from html_process.common.app_var import (
    SEMANTIC_ROLE,
    SEM_ROLE_PRODUCT_BOX,
)


def find(soup):
    """Find product box role."""
    all_images = soup.find_all("img")
    img_store = {}

    for img_tag in all_images:
        if tag_position.has_pos_attr(img_tag):
            this_pos = tag_position.this_get(img_tag)
            width = this_pos["width"]
            height = this_pos["height"]
            if width > 70 and height > 70:
                pos_index = str(width) + str(height)
                if pos_index in img_store:
                    img_store[pos_index].append(img_tag)
                else:
                    img_store[pos_index] = []
                    img_store[pos_index].append(img_tag)
    img_list = []
    for key in img_store:
        if len(img_store[key]) > len(img_list):
            img_list = img_store[key]

    for img_tag in img_list:
        img_tag.parent[SEMANTIC_ROLE] = SEM_ROLE_PRODUCT_BOX
        img_tag.parent["style"] = "border: black 1px solid !important"
