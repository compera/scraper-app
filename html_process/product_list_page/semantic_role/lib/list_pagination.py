"""List pagination container."""
from functools import reduce
from html_process.common import utils
from html_process.page.tag.tag_clickable.tag_clickable import (
    has_clickable_attr
)
from html_process.page.tag.tag_types.tag_type import (
    is_leaf_chain,
    get_leaf,
)
from html_process.common.app_var import (
    SEMANTIC_ROLE,
    SEM_ROLE_PAGINATION,
    SEM_ROLE_PAGINATION_ITEM,
    SEM_ROLE_CANDIDATE_PAGINATION,
    # SEMANTIC_TYPE,
    SEM_TYPE_NUMBER,
)


def _walker_up(parent):
    """
    Find all candidates.

    if element contian number of number type tags.
    """
    if(parent is not None and parent.name is not None):
        
        total_number_type = (
            len(parent.find_all(semantic_type=SEM_TYPE_NUMBER))
        )

        if(total_number_type >= 2):
            parent[SEMANTIC_ROLE] = SEM_ROLE_CANDIDATE_PAGINATION
            parent["style"] = "border: red 1px solid"
        else:
            _walker_up(parent.parent)


def _pagination_item_check(number_tag):
    """Do check if tag number is 1/2/3/4 and if clickable."""
    number = float(number_tag.string.strip())
    is_number = False
    is_clickable = has_clickable_attr(number_tag)
    numbers_list = [1, 2, 3, 4, 5, 6]
    for num in numbers_list:
        if num == number:
            is_number = True
            break

    return (is_number and is_clickable)


def _container_candidates(soup):
    for number_tag in soup.find_all(semantic_type=SEM_TYPE_NUMBER):
        if _pagination_item_check(number_tag):
            _walker_up(number_tag.parent)


def _mark_pagination_role(tag):
    tag[SEMANTIC_ROLE] = SEM_ROLE_PAGINATION
    tag["style"] = "border: red 3px solid"


def _mark_pagination_item(tag):
    tag[SEMANTIC_ROLE] = SEM_ROLE_PAGINATION_ITEM
    tag["style"] = "border: red 3px solid"


def _get_pagintion_item_leaf(item):
    if item and item.name is not None:
        if is_leaf_chain(item):
            return get_leaf(item)

    return item


def _pagination_item_mark_iteration(children_list):
    for item in children_list:
        if item and item.name is not None:
            item = _get_pagintion_item_leaf(item)
            if item.string and utils.is_number(item.string.strip()):
                _mark_pagination_item(item)


def _container_role(candidates):
    for candidate in candidates:
        # hold first 2 number tags to check if they sequences
        first_two_number = []
        children_list = list(candidate.children)
        for index, child in enumerate(children_list):
            if child and child.name is not None:
                # Sequences check
                child = _get_pagintion_item_leaf(child)
                if child.string and utils.is_number(child.string.strip()):
                    if len(first_two_number) < 2:
                        first_two_number.append(float(child.string.strip()))
                    else:
                        break

        if len(first_two_number) == 0:
            continue

        is_sequences = (
            True if
            reduce((lambda x, y: x + y), first_two_number) - 3 == 0
            else False
        )
        if is_sequences:
            _mark_pagination_role(candidate)
            _pagination_item_mark_iteration(children_list)


def find(soup):
    """Find list pagination role."""
    _container_candidates(soup)
    candidates = soup.find_all(semantic_role=SEM_ROLE_CANDIDATE_PAGINATION)
    _container_role(candidates)
