"""Basic dom processes."""
from bs4 import BeautifulSoup
from html_process.page.html_prep import html_prep
from html_process.page.processes import (
    leaf_process,
    leaf_chain_process,
    holder_process,
    holder_chain_process
)


def run(html_src):
    """Process html src code. return processed soup obj."""
    soup = BeautifulSoup(html_src, "lxml")
    html_prep.clean_html(soup)
    html_prep.fix_untagged_string(soup, soup)
    leaf_process.walker(soup.body)
    leaf_chain_process.process(soup.body)
    holder_process.walker(soup.body)
    holder_chain_process.process(soup.body)

    return soup
