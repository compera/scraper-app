"""Html prep."""
from bs4 import NavigableString, Comment


def clean_html(soup):
    """Clean noise tags from bs4 html tree and return a clean soup object."""
    tag_to_remove = ["script", "style", "noscript", "apm_do_not_touch"]
    for script in soup(tag_to_remove):
        script.decompose()

    for element in soup.find_all(text=lambda text: isinstance(text, Comment)):
        element.extract()


def fix_untagged_string(main_soup, soup):
    """Create new tag for the string, append it and remove source string.

    Keyword arguments:
    main_soup -- main soup object
    soup -- soup object to modify
    """
    if soup.name is not None:
        for child in soup.children:
            # in case first child is string
            if(isinstance(child.previous_sibling, NavigableString)):
                if(len(child.previous_sibling.string) > 1):
                    tag = main_soup.new_tag("span", alfred_tag="self")
                    tag.string = child.previous_sibling.extract()
                    child.insert_before(tag)

            if(isinstance(child.next_sibling, NavigableString)):
                if(len(child.next_sibling.string) > 1):
                    tag = main_soup.new_tag("span", alfred_tag="self")
                    tag.string = child.next_sibling.extract()
                    child.insert_after(tag)

            fix_untagged_string(main_soup, child)
