"""Leaf process run.

do: semantic type on leaf, tag_name, tag_clickable, tag_type-leaf.
"""
from html_process.page.semantic_type.leaf_semantic_type import (
    leaf_semantic_type
)
from html_process.page.tag.tag_name import tag_name
from html_process.page.tag.tag_clickable import tag_clickable
from html_process.page.tag.tag_types import leaf_tag_type


def walker(soup):
    """Leaf walker."""
    if soup.name is not None:
        for child in soup.children:
            # tag name
            tag_name.this_set(child)

            # tag clickable
            tag_clickable.process(child)

            # Tag type leaf
            leaf_tag_type.process(child)

            # Semantic type
            leaf_semantic_type.process(child)

            walker(child)
