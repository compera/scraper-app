"""Holder chain process run.

do: tag_type- holder_chain.
"""
from html_process.page.tag.tag_types import holder_chain_type
from html_process.page.tag.tag_types.tag_type import (
    get_holders,
)


def _walker_up(soup):
    """Iterate up over parents tag."""
    if soup.name is not None:
        is_holder_chain = holder_chain_type.process(soup)
        if(is_holder_chain):
            _walker_up(soup.parent)


def process(soup):
    """Process. get all holder and send it to walker up."""
    all_holders = get_holders(soup)
    for holder in all_holders:
        _walker_up(holder.parent)
