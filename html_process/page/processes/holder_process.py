"""Holder process run.

do: tag_type holder.
"""
from html_process.page.tag.tag_types import holder_type


def walker(soup):
    """Iterate over soup."""
    if soup.name is not None:
        holder_type.process(soup)
        for child in soup.children:
            walker(child)
