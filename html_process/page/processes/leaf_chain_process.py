"""Leaf chain process run.

do: semantic type on leaf chain, tag_type-leaf_chain.
"""
from html_process.page.tag.tag_types import leaf_chain_type
from html_process.page.tag.tag_types import leaf_tag_type


def _walker_up(soup):
    """Iterate up over parents tag."""
    if soup.name is not None:
        is_leaf_chain = leaf_chain_type.process(soup)
        if(is_leaf_chain):
            _walker_up(soup.parent)


def process(soup):
    """Process. get all tag_type leaf and send it to walker_up."""
    all_leafs = leaf_tag_type.get_all_leafs(soup)
    for leaf in all_leafs:
        _walker_up(leaf.parent)
