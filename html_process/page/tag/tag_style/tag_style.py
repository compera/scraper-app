"""Tag style."""
import json
from html_process.common import utils
from html_process.common.app_var import (
    TAG_STYLE,
)


def prep_font_size_param(font_size):
    """Convert font-size to a number."""
    return float(font_size.replace("px", ""))


def prep_text_decoration_param(text_decoration):
    """Convert text_decoration to a number."""
    return 0 if text_decoration == "line-through" else 1


def prep_font_weight_param(font_weight):
    """Convert font_weight to a number."""
    value = 0
    if(utils.is_number(font_weight) is False):
        if(font_weight == "normal"):
            value = 400
        elif(font_weight == "bold"):
            value = 500
    else:
        value = float(font_weight)

    return value


def find_parent_with_style(tag):
    """Check parent until tag style found."""
    if(tag.name is not None and tag.has_attr(TAG_STYLE)):
        return tag
    else:
        return find_parent_with_style(tag.parent)


def this_get(tag):
    """Return style obj from tag element.

    if tag have not style attr return parent obj.
    """
    _tag = tag if tag.has_attr(TAG_STYLE) else tag.parent
    return json.loads(_tag[TAG_STYLE])
