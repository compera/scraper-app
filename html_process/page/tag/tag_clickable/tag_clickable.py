"""Add tag name."""
from html_process.common.app_var import (
    TAG_CLICKABLE,
)
from html_process.page.tag.tag_style import tag_style


def has_clickable_attr(tag):
    """Return true if tag is clickable."""
    return tag.has_attr(TAG_CLICKABLE)


def _is_clickable(tag):
    """Check if element is clickable.

    Keyword arguments:
    tag -- soup tag element
    """
    tag_names_clickable = ["input", "option", "a", "button", "i"]
    style = tag_style.this_get(tag_style.find_parent_with_style(tag))
    cursor = style["cursor"]

    return (
        cursor == "pointer" or
        tag.name == tag_names_clickable[0] or
        tag.name == tag_names_clickable[1] or
        tag.name == tag_names_clickable[2] or
        tag.name == tag_names_clickable[3] or
        tag.name == tag_names_clickable[4]
    )


def this_set(tag):
    """Set tag clickable."""
    if tag.name is not None:
        tag[TAG_CLICKABLE] = "true"


def process(tag):
    """Check if element is clickable and set accordingly."""
    if tag.name is not None:
        clickable = _is_clickable(tag)
        if(clickable):
            this_set(tag)
