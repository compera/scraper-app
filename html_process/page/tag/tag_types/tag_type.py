"""Tag_type."""
from html_process.common.app_var import (
    TAG_TYPE,
    T_TYPE_LEAF,
    T_TYPE_LEAF_CHAIN,
    T_TYPE_HOLDER,
    T_TYPE_HOLDER_CHAIN,
)


def get_holders(soup):
    """Return list of all holders tags."""
    return soup.find_all(tag_type=(T_TYPE_HOLDER))


def is_leaf(tag):
    """Return true if tag is leaf."""
    return (
        tag[TAG_TYPE] == T_TYPE_LEAF if
        tag.has_attr(TAG_TYPE) else
        False
    )


def is_leaf_chain(tag):
    """Return true if tag is leaf_chain."""
    return (
        tag[TAG_TYPE] == T_TYPE_LEAF_CHAIN if
        tag.has_attr(TAG_TYPE) else
        False
    )

def is_holder(tag):
    """Return true if tag is holder."""
    return (
        tag[TAG_TYPE] == T_TYPE_HOLDER if
        tag.has_attr(TAG_TYPE) else
        False
    )

def tag_is_leaf_or_holder(tag):
    """Return true if tag is leaf, or holder type."""
    res = False
    if(tag.has_attr(TAG_TYPE)):
        _type = tag[TAG_TYPE]
        if _type == T_TYPE_LEAF:
            res = True
        if _type == T_TYPE_LEAF_CHAIN:
            res = True
        if _type == T_TYPE_HOLDER:
            res = True
        if _type == T_TYPE_HOLDER_CHAIN:
            res = True

    return res


def get_leaf(leaf_chain_tag):
    """Return leaf tag of the chain."""
    return leaf_chain_tag.find(tag_type=(T_TYPE_LEAF))

