"""Leaf tag type."""
from bs4 import NavigableString
from html_process.common.app_var import (
    TAG_TYPE,
    T_TYPE_LEAF,
)


def _is_leaf(tag, children_count):
    """Check if children of the element is 0 or have 1 string type."""
    is_leaf = False
    if(children_count == 0):
        is_leaf = True
    if(children_count == 1):
        string_tag = isinstance(tag.contents[0], NavigableString)
        if(string_tag):
            is_leaf = True

    return is_leaf


def this_set(tag):
    """Set tag type to leaf."""
    if tag.name is not None:
        tag[TAG_TYPE] = T_TYPE_LEAF


def process(tag):
    """Check if element is leaf and set accordingly."""
    if tag.name is not None:
        children_count = len(tag.contents)
        if(_is_leaf(tag, children_count)):
            this_set(tag)


def get_all_leafs(soup):
    """Return list of all leafs tags."""
    return soup.find_all(tag_type=(T_TYPE_LEAF))
