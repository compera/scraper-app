"""Leaf chain tag type."""
from html_process.common import utils
from html_process.common.app_var import (
    TAG_TYPE,
    T_TYPE_LEAF,
    T_TYPE_LEAF_CHAIN,
)


def _is_leaf_chain(tag):
    """Check parents of leaf_chain, return true if tag contain 1 leaf."""
    children_count = utils.real_children_count(tag)
    return True if children_count == 1 else False


def this_set(tag):
    """Set tag type to leaf_chain."""
    if tag.name is not None:
        tag[TAG_TYPE] = T_TYPE_LEAF_CHAIN


def process(tag):
    """
    Check if element is leaf_chain and set accordingly.

    Return true if is_leaf chain found else false
    """
    if tag.name is not None:
        if(_is_leaf_chain(tag)):
            this_set(tag)
            return True
        else:
            return False
