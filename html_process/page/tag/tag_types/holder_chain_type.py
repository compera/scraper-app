"""Holder chain tag type."""
from html_process.common.app_var import (
    TAG_TYPE,
    T_TYPE_HOLDER,
    T_TYPE_HOLDER_CHAIN
)


def _is_holder_chain(tag):
    """Tag contain leaf_holder."""
    child_count = 0
    child_holder_count = 0
    for child in tag.children:
        if(child.name is not None):
            child_count += 1
            if(child.has_attr(TAG_TYPE)):
                child_tag_type = child[TAG_TYPE]
                if(child_tag_type == T_TYPE_HOLDER):
                    child_holder_count += 1

    return (
        True if
        (child_count == 1 and child_holder_count == 1)
        else False
    )


def this_set(tag):
    """Set tag type to holder_chain."""
    if tag.name is not None:
        tag[TAG_TYPE] = T_TYPE_HOLDER_CHAIN


def process(tag):
    """
    Check if element is holder_chain and set accordingly.

    Return true if holder chain found else false
    """
    if tag.name is not None:
        if(_is_holder_chain(tag)):
            this_set(tag)
            return True
        else:
            return False
