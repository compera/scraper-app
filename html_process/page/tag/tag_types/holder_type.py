"""Holder tag type."""
from html_process.common.app_var import (
    TAG_TYPE,
    T_TYPE_LEAF,
    T_TYPE_LEAF_CHAIN,
    T_TYPE_HOLDER,
)


def _is_holder(tag):
    """Tag contains at least 2 leaf/chain leaf tags."""
    child_count = 0
    child_leaf_count = 0
    for child in tag.children:
        if(child.name is not None):
            child_count += 1
            if(child.has_attr(TAG_TYPE)):
                _type = child[TAG_TYPE]
                leaf_or_chain = (
                    _type == T_TYPE_LEAF or _type == T_TYPE_LEAF_CHAIN
                )
                if (leaf_or_chain):
                    child_leaf_count += 1

    return (
        True if
        (child_count > 1 and child_count - child_leaf_count == 0)
        else False
    )


def this_set(tag):
    """Set tag type to holder."""
    if tag.name is not None:
        tag[TAG_TYPE] = T_TYPE_HOLDER


def process(tag):
    """Check if element is holder and set accordingly."""
    if tag.name is not None:
        if(_is_holder(tag)):
            this_set(tag)
