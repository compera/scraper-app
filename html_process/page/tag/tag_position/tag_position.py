"""Tag position."""
import json
from html_process.common.app_var import (
    TAG_POSITION,
)


def has_pos_attr(tag):
    """Return if tag has tag_position."""
    return tag.has_attr(TAG_POSITION)


def find_parent_with_pos(tag):
    """Check parent until tag position found."""
    if(tag.name is not None and tag.has_attr(TAG_POSITION)):
        return tag
    else:
        return find_parent_with_pos(tag.parent)


def this_get(tag):
    """Return position obj from tag element.

    if tag have not position attr return parent obj.
    """
    _tag = tag if tag.has_attr(TAG_POSITION) else tag.parent
    return json.loads(_tag[TAG_POSITION])
