"""Add tag name."""
from html_process.common.app_var import (
    TAG_NAME,
)


def this_set(tag):
    """Set tag name."""
    if tag.name is not None:
        tag[TAG_NAME] = tag.name
