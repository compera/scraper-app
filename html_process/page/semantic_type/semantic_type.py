"""Semantic type."""
from html_process.common.app_var import (
    TAG_TYPE,
    SEMANTIC_TYPE,
    T_TYPE_LEAF,
    T_TYPE_LEAF_CHAIN,
    SEM_TYPE_NUMBER,
    SEM_TYPE_NUMBER_WORD,
)


def get_all_numbers(soup):
    """Return list of all semantic number type."""
    return soup.find_all(semantic_type=(SEM_TYPE_NUMBER))


def get_leaf_semantic_type(tag):
    """Return leaf semantic type.

    Check the tag type and if leaf_chain get the leaf semantic type inside.
    If no tag_type and no semantic_type return None.
    """
    if(tag.has_attr(TAG_TYPE) is False):
        return None

    _tag_type = tag[TAG_TYPE]
    if(_tag_type == T_TYPE_LEAF):
        return tag[SEMANTIC_TYPE] if tag.has_attr(SEMANTIC_TYPE) else "None"

    if(_tag_type == T_TYPE_LEAF_CHAIN):
        return get_leaf_semantic_type(tag.find(tag_type=(T_TYPE_LEAF)))


def get_all_number_and_word(soup):
    """Return list of all semantic number and word type."""
    return soup.find_all(semantic_type=(SEM_TYPE_NUMBER_WORD))
