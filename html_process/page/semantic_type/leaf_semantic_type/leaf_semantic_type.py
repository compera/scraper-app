"""Semantic type for leaf tags."""
from bs4 import NavigableString
from html_process.common.currency_symbols import currency_list
from html_process.common import utils
from html_process.common.app_var import (
    SEMANTIC_TYPE,
    SEM_TYPE_PRICE,
    SEM_TYPE_EMPTY,
    SEM_TYPE_TITLE,
    SEM_TYPE_NUMBER,
    SEM_TYPE_NUMBER_WORD,
    SEM_TYPE_DESC
)


def _price_type_check(str_leaf):
    """Check if string start with curency symbol."""
    # TODO: support more currency types
    _list = currency_list
    is_currency = False
    for symbol in _list:
        if str_leaf.strip().startswith(symbol):
            is_currency = True

    return is_currency
    # return True if str_leaf.strip().startswith('$') else False


def _price_type_mark(str_leaf):
    """Mark this string has price."""
    str_leaf.parent[SEMANTIC_TYPE] = SEM_TYPE_PRICE
    str_leaf.parent["style"] = "border-bottom: blue 1px solid"


def _empty_type_check(leaf):
    """Check if string is empty."""
    return True if len(leaf.contents) <= 0 else False


def _empty_type_mark(leaf):
    """Mark parent string for empty type."""
    leaf[SEMANTIC_TYPE] = SEM_TYPE_EMPTY


def _title_type_check(str_leaf):
    """Check if string is title type."""
    string_count = 0
    capitalized_count = 0

    for w in str_leaf.split(" "):
        word = w.strip()
        word_valid = True if isinstance(word, str) and len(word) > 0 else False
        if(word_valid and utils.is_number(word) is False):
            string_count += 1
            is_capitalized = word[0].isupper()
            if(is_capitalized):
                capitalized_count += 1

    return string_count > 0 and capitalized_count >= (string_count / 3)


def _desc_type_mark(str_leaf):
    """Mark parent string for description type."""
    str_leaf.parent[SEMANTIC_TYPE] = SEM_TYPE_DESC
    str_leaf.parent["style"] = "border-bottom: black 1px solid !important"


def _desc_type_check(str_leaf):
    """Check if string is description type."""
    return len(str_leaf.split(" ")) >= 20


def _title_type_mark(str_leaf):
    """Mark parent string for title type."""
    str_leaf.parent[SEMANTIC_TYPE] = SEM_TYPE_TITLE
    str_leaf.parent["style"] = "border-bottom: green 1px solid"


def _number_type_check(str_leaf):
    """Check if string is number type using utils method."""
    return utils.is_number(str_leaf)


def _number_type_mark(str_leaf):
    """Mark parent string for number type."""
    str_leaf.parent[SEMANTIC_TYPE] = SEM_TYPE_NUMBER
    str_leaf.parent["style"] = "border-bottom: orange 1px solid"


def _number_and_word_type_mark(str_leaf):
    """Mark parent string for number and word type."""
    str_leaf.parent[SEMANTIC_TYPE] = SEM_TYPE_NUMBER_WORD
    str_leaf.parent["style"] = "border-bottom: pink 1px solid"


def _number_and_word_type_check(str_leaf):
    """Check if string is number and word type.

    Number and word type - max length of 6 words.
    """
    string_count = 0
    word_count = 0
    number_count = 0

    string_split = str_leaf.strip().split(" ")
    if(len(string_split) == 1):
        string_split = str_leaf.strip().replace("/", " / ")

    for s in string_split:
        st = s.strip()
        st_valid = True if isinstance(st, str) and len(st) > 0 else False
        if(st_valid):
            string_count += 1
            if(utils.is_number(st)):
                number_count += 1
            else:
                word_count += 1

    word_number_ratio = -1
    if(string_count <= 5 and word_count > 0 and number_count > 0):
        word_number_ratio = (
            word_count/number_count if
            word_count < number_count
            else
            number_count/word_count
        )

    return word_number_ratio > 0.33


def process(leaf_tag):
    """Process one tag and mark his semantic type."""
    if(isinstance(leaf_tag, NavigableString)):
        if(_price_type_check(leaf_tag)):
            _price_type_mark(leaf_tag)
        elif(_number_and_word_type_check(leaf_tag)):
            _number_and_word_type_mark(leaf_tag)
        elif(_title_type_check(leaf_tag)):
            _title_type_mark(leaf_tag)
        elif(_number_type_check(leaf_tag)):
            _number_type_mark(leaf_tag)
        elif(_desc_type_check(leaf_tag)):
            _desc_type_mark(leaf_tag)
    else:
        if(_empty_type_check(leaf_tag)):
            _empty_type_mark(leaf_tag)


def get_all_titles(soup):
    """Return list of all semantic title type."""
    return soup.find_all(semantic_type=(SEM_TYPE_TITLE))


def get_all_prices(soup):
    """Return list of all semantic prices type."""
    return soup.find_all(semantic_type=(SEM_TYPE_PRICE))


def get_all_desc(soup):
    """Return list of all semantic desc type."""
    return soup.find_all(semantic_type=(SEM_TYPE_DESC))
