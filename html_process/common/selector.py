import itertools


def xpath(element):
    """
    Generate xpath of soup element.

    :param element: bs4 text or node
    :return: xpath as string
    """
    components = []

    child = element if element.name else element.parent
    for parent in child.parents:
        """
        @type parent: bs4.element.Tag
        """
        previous = itertools.islice(
            parent.children,
            0,
            parent.contents.index(child)
        )
        xpath_tag = child.name
        xpath_index = sum(1 for i in previous if i.name == xpath_tag) + 1
        # components.append(
        #     xpath_tag if
        #     xpath_index == 1
        #     else '%s[%d]' % (xpath_tag, xpath_index)
        # )
        components.append('%s[%d]' % (xpath_tag, xpath_index))
        child = parent
    components.reverse()

    return '/%s' % '/'.join(components)


def css_selector(element):
    """
    Generate css selector of soup element.

    :param element: bs4 text or node
    :return: css-selector as string
    """
    child = element
    class_attributes = child["class"] if child.has_attr("class") else []

    selector = "."
    for c in class_attributes:
        selector += c

    return selector
