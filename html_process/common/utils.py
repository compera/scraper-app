"""Common methods."""


def is_number(s):
    """Check if value is number.

    Keyword arguments:
    s -- value string
    """
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False


def real_children_count(tag):
    """Get correct number of children, count valid tags names."""
    count = 0
    for child in tag.children:
        if(child.name is not None):
            count += 1

    return count


def is_on_screen(pos):
    """Check if element is visible on page.

    Keyword arguments:
    pos -- TAG_POSITION object
    """
    for key, value in pos.items():
        if(value != 0):
            return True

    return False
