import os
import json
# import re


def get_files():
    files = os.listdir("../data_ready_for_html_process")
    files_path_list = []
    for f in files:
        file_path = "../data_ready_for_html_process" + "/" + f
        files_path_list.append(file_path)

    return files_path_list


def get_html_data(file_path):
    file_obj_data = open(file_path, "r")
    data = json.load(file_obj_data)

    return {
        "src": data["html_src"].replace("\\n", '').replace("\\t", '')
    }
