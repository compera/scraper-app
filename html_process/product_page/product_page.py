"""Run all process for product page."""
from bs4 import BeautifulSoup
from html_process.page import page
from html_process.common.app_var import (
    SEM_ROLE_TITLE,
    SEM_ROLE_PRICE,
)
from html_process.common import (
    selector,
)
from html_process.page.semantic_type.leaf_semantic_type import (
    leaf_semantic_type
)
from html_process.product_page.semantic_role import (
    semantic_role,
    product_title,
    product_price,
    product_image,
    main_container,
)

from html_process.product_page.semantic_role.product_variations import (
    product_variations,
)
from html_process.product_page.semantic_role.product_rating import (
    product_rating
)
from html_process.product_page.semantic_role.product_info import product_info


def run(html_src, model_variation):
    """Process html src code. return processed soup obj."""
    soup = page.run(html_src)
    product_title.find(
        soup,
        leaf_semantic_type.get_all_titles(soup),
        soup.title.string.strip()
    )
    product_price.find(soup, leaf_semantic_type.get_all_prices(soup))
    product_image.find(soup)
    main_container.find(soup)
    product_variations.find(soup, model_variation)
    product_rating.find(soup)
    # product_info.find(soup)

    return soup


def export_to_html(soup):
    """Return html src code."""
    return soup.prettify()


def get_title_xpath(soup):
    """Return xpath."""
    title = soup.find(semantic_role=(SEM_ROLE_TITLE))
    return selector.xpath(title)


def get_price_xpath(soup):
    """Return xpath."""
    price = soup.find(semantic_role=(SEM_ROLE_PRICE))
    return selector.xpath(price)


def get_on_page_variation(soup):
    """Return variation object."""
    return product_variations.on_page_data_extraction(soup)


def get_clickable_variations_list(soup):
    """Return clickable variation object."""
    return product_variations.get_clickable_items(soup)


def get_product_rating(soup):
    """Return product rating, return number."""
    return product_rating.product_rating_extract(soup)


def get_product_info(soup):
    """Return product info, return object."""
    return product_info.extract(soup)


def get_product_price(soup):
    """Return product price."""
    return semantic_role.product_price_extract(soup)


def get_product_title(soup):
    """Return product title."""
    return semantic_role.product_title_extract(soup)


def get_product_image_src(soup):
    image_src = semantic_role.product_image_extract_src(soup)
    if type(image_src) is str and image_src.startswith("//"):
        image_src = image_src.replace("//", "https://")

    return image_src