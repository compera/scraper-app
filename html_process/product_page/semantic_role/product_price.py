"""Find product price role."""
from html_process.common import utils
from html_process.page.tag.tag_position import tag_position
from html_process.page.tag.tag_style import tag_style
from html_process.product_page.semantic_role.semantic_role import (
    get_product_title_tag
)
from html_process.common.app_var import (
    SEMANTIC_ROLE_CANDIDATE,
    SEM_ROLE_CANDIDATE_PRICE,
    SEMANTIC_ROLE,
    SEM_ROLE_PRICE,
)


def _get_top_pos_product_price_value(all_prices):
    """Get the value of the topest price found."""
    top = -1
    for candidate in all_prices:
        this_pos = tag_position.this_get(candidate)
        this_top = this_pos["top"]
        if(top == -1):
            top = this_top
        elif(this_top < top):
            top = this_top

    return top


def _product_price_score(pos_top, font_size, text_decoration, font_weight):
    """Style method score, return score number."""
    param_top = float(pos_top)
    param_font_size = tag_style.prep_font_size_param(font_size)
    param_text_decor = tag_style.prep_text_decoration_param(text_decoration)
    param_font_weight = tag_style.prep_font_weight_param(font_weight)

    score = (
        (param_font_size * param_text_decor * param_font_weight) /
        param_top
    )
    return score


def _candidates(price_type):
    """Process candidates."""
    tag = price_type

    if(tag.name is None):
        tag = price_type.parent

    if(tag_position.has_pos_attr(tag) is False):
        return

    tag_pos = tag_position.this_get(tag)

    if(utils.is_on_screen(tag_pos)):
        tag[SEMANTIC_ROLE_CANDIDATE] = SEM_ROLE_CANDIDATE_PRICE
        tag["style"] = "border: blue 1px solid !important"


def _check_candidate_related_to_title_role(candidate_pos, title_role_pos):
    return candidate_pos["top"] <= title_role_pos["top"]


def _score_method(all_potential_product_price, title_role_pos):
    """Style method for product price - role step."""
    current_best_candidate = None
    best_score = -1

    for candidate in all_potential_product_price:

        this_pos = tag_position.this_get(candidate)
        this_al_style = tag_style.this_get(candidate)

        # check if this candidate is under title role
        if(_check_candidate_related_to_title_role(this_pos, title_role_pos)):
            continue

        score = _product_price_score(
            this_pos["top"],
            this_al_style["font-size"],
            this_al_style["text-decoration"],
            this_al_style["font-weight"],
        )
        # print("score: ", str(score))
        if(score > best_score):
            if(current_best_candidate is not None):
                current_best_candidate["semantic_type"] = ""
                current_best_candidate["style"] = (
                    "border: 1px blue solid !important"
                )
            best_score = score
            candidate[SEMANTIC_ROLE] = SEM_ROLE_PRICE
            candidate["style"] = "border: 3px blue solid !important"
            current_best_candidate = candidate


def find(soup, all_price_type):
    """Find product price."""
    title_role_pos = tag_position.this_get(get_product_title_tag(soup))

    for price_type in all_price_type:
        _candidates(price_type)

    all_potential_product_price = soup.find_all(
        semantic_role_candidate=(SEM_ROLE_CANDIDATE_PRICE)
    )
    _score_method(all_potential_product_price, title_role_pos)
