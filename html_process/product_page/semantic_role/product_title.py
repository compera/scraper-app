"""Find product title role."""
import jellyfish
from html_process.common import utils
from html_process.page.tag.tag_position import tag_position
from html_process.page.tag.tag_style import tag_style
from html_process.common.app_var import (
    SEMANTIC_ROLE_CANDIDATE,
    SEM_ROLE_CANDIDATE_TITLE,
    SEMANTIC_ROLE,
    SEM_ROLE_TITLE,
)


def _distance(meta_title, candidate):
    """Measure distance between meta_title and title semantic type."""
    distance_score = jellyfish.jaro_distance(meta_title, candidate)
    return True if distance_score > 0.75 else False


def _meta_title_method_candidate(title_type, meta_title):
    """Meta title method - candidates step."""
    tag = title_type

    if(tag.name is None):
        tag = title_type.parent

    tag_pos = tag_position.this_get(tag_position.find_parent_with_pos(tag))

    is_on_screen = utils.is_on_screen(tag_pos)
    str_length_valid = len(tag.string.strip()) > 8
    distance = _distance(meta_title, tag.string.strip())
    if (is_on_screen and str_length_valid and distance):
        tag[SEMANTIC_ROLE_CANDIDATE] = SEM_ROLE_CANDIDATE_TITLE
        tag["style"] = "border: green 1px solid !important"


def _score(pos_top, font_weight, text_decoration, string_length):
    """Style method - return score for title."""
    param_top = float(pos_top)
    param_text_deco = tag_style.prep_text_decoration_param(text_decoration)
    param_font_weight = tag_style.prep_font_weight_param(font_weight)
    param_string_length = string_length if string_length is not None else 1

    score = (
        (param_text_deco * param_font_weight * param_string_length) / param_top
        if param_top > 0
        else 0
    )

    return score


def _style_pos_method_process(all_potential_main_title):
    """Style method - role step."""
    current_best_candidate = None
    current_font_size = -1
    best_score = -1

    for candidate in all_potential_main_title:
        this_tag_pos = tag_position.this_get(
            tag_position.find_parent_with_pos(candidate)
        )
        if(utils.is_on_screen(this_tag_pos)):
            this_tag_style = tag_style.this_get(candidate)
            this_font_size = tag_style.prep_font_size_param(
                this_tag_style["font-size"]
            )

            if(this_font_size > current_font_size):
                if(current_best_candidate is not None):
                    current_best_candidate[SEMANTIC_ROLE] = ""
                    current_best_candidate["style"] = (
                        "border: 1px green solid !important"
                    )
                current_font_size = this_font_size
                best_score = _score(
                    this_tag_pos["top"],
                    this_tag_style["font-weight"],
                    this_tag_style["text-decoration"],
                    len(candidate.string)
                )
                candidate[SEMANTIC_ROLE] = SEM_ROLE_TITLE
                candidate["style"] = "border: green 3px solid !important"
                current_best_candidate = candidate

            elif(this_font_size == current_font_size):
                this_score = _score(
                    this_tag_pos["top"],
                    this_tag_style["font-weight"],
                    this_tag_style["text-decoration"],
                    len(candidate.string)
                )
                if(this_score > best_score):
                    if(current_best_candidate is not None):
                        current_best_candidate[SEMANTIC_ROLE] = ""
                        current_best_candidate["style"] = (
                            "border: 1px green solid !important"
                        )
                    current_font_size = this_font_size
                    best_score = _score(
                        this_tag_pos["top"],
                        this_tag_style["font-weight"],
                        this_tag_style["text-decoration"],
                        len(candidate.string)
                    )
                    candidate[SEMANTIC_ROLE] = SEM_ROLE_TITLE
                    candidate["style"] = "border: green 3px solid !important"
                    current_best_candidate = candidate


def find(soup, all_title_type, meta_title):
    """Find product title."""
    for title_type in all_title_type:
        _meta_title_method_candidate(title_type, meta_title)

    all_potential_main_title = soup.find_all(
        semantic_role_candidate=(SEM_ROLE_CANDIDATE_TITLE)
    )

    if(len(all_potential_main_title) > 0):
        _style_pos_method_process(all_potential_main_title)
    # do second method style_pos
    else:
        _style_pos_method_process(all_title_type)
