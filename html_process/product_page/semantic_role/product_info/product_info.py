"""Product info."""
from html_process.product_page.semantic_role.product_info.lib import (
    process,
    data_extracor
)
from html_process.common.app_var import (
    SEM_ROLE_INFO_CONTAINER,
)


def extract(soup):
    """Product info extracor warpper."""
    container_list = soup.find_all(semantic_role=SEM_ROLE_INFO_CONTAINER)
    return data_extracor.extract(container_list)


def find(soup):
    """Product info."""
    process.find(soup)
