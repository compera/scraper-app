"""Extract product info."""
from html_process.common.app_var import (
    SEMANTIC_ROLE,
    SEM_ROLE_INFO_ITEM_LEFT,
    SEM_ROLE_INFO_ITEM_RIGHT,
    SEM_ROLE_INFO_ITEM_HOLDER,
)


def _item_process(left_item_tag):
    right_item_tag = None
    for sibiling in left_item_tag.next_siblings:
        if sibiling and sibiling.name is not None:
            if(sibiling.has_attr(SEMANTIC_ROLE) and
               sibiling[SEMANTIC_ROLE] == SEM_ROLE_INFO_ITEM_RIGHT):
                right_item_tag = sibiling
                break

    left_item = left_item_tag.string.strip()
    right_item = right_item_tag.string.strip()

    return (left_item, right_item)


def _extract_container(container):
    info = {}
    for child in container.children:
        if child.name is not None:
            if(child.has_attr(SEMANTIC_ROLE)):
                _role = child[SEMANTIC_ROLE]

                # case container hold item holders
                if _role == SEM_ROLE_INFO_ITEM_HOLDER:
                    for child_child in child.children:
                        if child_child.name is not None:
                            if(child_child.has_attr(SEMANTIC_ROLE)):
                                child_role = child_child[SEMANTIC_ROLE]
                                if child_role == SEM_ROLE_INFO_ITEM_LEFT:
                                    left_right_info = (
                                        _item_process(child_child)
                                    )
                                    key = left_right_info[0]
                                    value = left_right_info[1]
                                    info[key] = value

                if _role == SEM_ROLE_INFO_ITEM_LEFT:
                    left_right_info = _item_process(child)
                    key = left_right_info[0]
                    value = left_right_info[1]
                    info[key] = value

    return info


def extract(containers_list):
    """Extract product info, return object with each container separate."""
    info_obj = {}
    for index, container in enumerate(containers_list):
        info_obj[str(index + 1)] = _extract_container(container)

    return info_obj
