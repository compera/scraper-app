"""Find product info candidates."""
from html_process.common.utils import (
    is_on_screen,
)
from html_process.page.tag.tag_types.tag_type import (
    get_holders,
)
from html_process.page.semantic_type.semantic_type import (
    get_leaf_semantic_type
)
from html_process.page.tag.tag_clickable import tag_clickable
from html_process.page.tag.tag_position import tag_position
from html_process.common.app_var import (
    SEM_TYPE_EMPTY,
    SEM_TYPE_PRICE,
    AL_TAG,
    SEMANTIC_ROLE,
    SEMANTIC_ROLE_CANDIDATE,
    SEM_ROLE_INFO_ITEM_LEFT,
    SEM_ROLE_INFO_ITEM_RIGHT,
    SEM_ROLE_INFO_ITEM_HOLDER,
    SEM_ROLE_INFO_CONTAINER,
)


def _info_item_left_mark(tag):
    """Mark product info item left side role."""
    tag[SEMANTIC_ROLE] = SEM_ROLE_INFO_ITEM_LEFT
    tag["style"] = "border: purple 1px solid !important"


def _info_item_right_mark(tag):
    """Mark product info item right side role."""
    tag[SEMANTIC_ROLE] = SEM_ROLE_INFO_ITEM_RIGHT
    tag["style"] = "border: purple 1px solid !important"


def _info_item_holder_mark(tag):
    """Mark product info holder."""
    tag[SEMANTIC_ROLE] = SEM_ROLE_INFO_ITEM_HOLDER


def _info_container_mark(tag):
    """Mark product info container role."""
    tag[SEMANTIC_ROLE] = SEM_ROLE_INFO_CONTAINER
    tag["style"] = "border: purple 3px dotted !important"


def _tags_related(tag1, tag2):
    """Related pos.

    Check two tags are in product info items shape. L -> R.
    """
    # if next tag is alfred_tag
    if(tag1.has_attr(AL_TAG) or tag2.has_attr(AL_TAG)):
        return True

    tag1_pos = tag_position.this_get(tag_position.find_parent_with_pos(tag1))
    tag2_pos = tag_position.this_get(tag_position.find_parent_with_pos(tag2))

    # tag1_top = tag1_pos["top"]
    # tag1_height = tag1_pos["height"]
    # tag2_top = tag2_pos["top"]
    # tag2_height = tag2_pos["height"]

    # top_check = abs(tag1_top - tag2_top) == 0
    # heigth_check = abs(tag1_height - tag2_height) <= 10

    # return bool(top_check and heigth_check)

    tag1_top = tag1_pos["top"]
    tag1_right = tag1_pos["right"]
    tag2_top = tag2_pos["top"]
    tag2_left = tag2_pos["left"]

    top_check = abs(tag1_top - tag2_top) <= 10
    right_to_left_check = abs(tag1_right - tag2_left) <= 40

    return bool(top_check and right_to_left_check)


def _info_items_pairs_condition(leaf, leaf_sibiling):
    """Condition breaker for _info_items_pairs."""
    if leaf is None or leaf_sibiling is None:
        return True

    if leaf.name is None or leaf_sibiling.name is None:
        return True

    leaf_type = get_leaf_semantic_type(leaf)
    leaf_sibiling_type = get_leaf_semantic_type(leaf_sibiling)

    # empty type tag
    if leaf_type == SEM_TYPE_EMPTY or leaf_sibiling_type == SEM_TYPE_EMPTY:
        return True

    # price type tag
    if leaf_type == SEM_TYPE_PRICE or leaf_sibiling_type == SEM_TYPE_PRICE:
        return True

    leaf_is_clickable = tag_clickable.has_clickable_attr(leaf)
    leaf_sibiling_is_clickable = (
        tag_clickable.has_clickable_attr(leaf_sibiling)
    )

    if leaf_is_clickable or leaf_sibiling_is_clickable:
        return True

    # leaf is on screen
    if tag_position.has_pos_attr(leaf):
        leaf_pos = tag_position.this_get(leaf)
        if is_on_screen(leaf_pos) is False:
            return True

    # check if tags are from compare tables
    if leaf.string and leaf_sibiling.string:
        if leaf.string.strip() == leaf_sibiling.string.strip():
            return True

    # leaf_sibiling is on screen
    if tag_position.has_pos_attr(leaf_sibiling):
        leaf_sibiling_pos = tag_position.this_get(leaf_sibiling)
        if is_on_screen(leaf_sibiling_pos) is False:
            return True

    # Semantic role checks
    if leaf.has_attr(SEMANTIC_ROLE) or leaf.has_attr(SEMANTIC_ROLE_CANDIDATE):
        return True

    if(leaf_sibiling.has_attr(SEMANTIC_ROLE) or
       leaf_sibiling.has_attr(SEMANTIC_ROLE_CANDIDATE)):
        return True

    return False


def _get_next_sibiling(leaf):
    """
    Return real sibiling.

    Fix BeautifulSoup method -
    returning None sibling even if there is sibling.
    """
    for sibiling in leaf.next_siblings:
        if sibiling and sibiling.name is not None:
            return sibiling

    return None


def _info_items_pairs(holder):
    """Check holders children if they paris- left item -> right item."""
    number_of_pairs = 0
    for leaf in holder.children:
        if leaf.name is not None:
            next_sibling = _get_next_sibiling(leaf)
            if(_info_items_pairs_condition(leaf, next_sibling)):
                continue

            related = _tags_related(leaf, next_sibling)
            if related:
                number_of_pairs += 1
                _info_item_left_mark(leaf)
                _info_item_right_mark(next_sibling)

    # mark info holder
    if(number_of_pairs == 1):
        _info_item_holder_mark(holder)


def _holder_pair_ratio(holder, sem_role_to_check):
    children_count = 0
    number_of_holder_pairs = 0

    for child in holder.children:
        if child.name is not None:
            if get_leaf_semantic_type(child) != SEM_TYPE_EMPTY:
                children_count += 1
            if child.has_attr(SEMANTIC_ROLE):
                if child[SEMANTIC_ROLE] == sem_role_to_check:
                    number_of_holder_pairs += 1

    return (
        (number_of_holder_pairs / children_count)
        if children_count > 1 and number_of_holder_pairs > 2
        else 0
    )


def _pair_ratio(holder, left_side_role, right_side_role):
    children_count = 0
    number_of_items = 0

    for child in holder.children:
        if child.name is not None:
            if get_leaf_semantic_type(child) != SEM_TYPE_EMPTY:
                children_count += 1
            if child.has_attr(SEMANTIC_ROLE):
                is_info_item = (
                    child[SEMANTIC_ROLE] == left_side_role or
                    child[SEMANTIC_ROLE] == right_side_role
                )
                if is_info_item:
                    number_of_items += 1

    return (
        (number_of_items / children_count)
        if children_count > 1 and number_of_items > 4
        else 0
    )


def _container_process(left_items):
    """Find product info container."""
    for item in left_items:
        parent = item.parent
        parent_semantic_role = (
            parent[SEMANTIC_ROLE] if parent.has_attr(SEMANTIC_ROLE) else ""
        )
        # case parent alerady marked
        if(parent_semantic_role == SEM_ROLE_INFO_CONTAINER):
            continue

        # case parent is item holder
        if(parent_semantic_role == SEM_ROLE_INFO_ITEM_HOLDER):

            this_holder_parent = parent.parent
            pairs_ratio = _holder_pair_ratio(
                this_holder_parent,
                SEM_ROLE_INFO_ITEM_HOLDER
            )
            if pairs_ratio > 0.75:
                _info_container_mark(this_holder_parent)
        else:
            # check parent
            pairs_ratio = _pair_ratio(
                parent,
                SEM_ROLE_INFO_ITEM_LEFT,
                SEM_ROLE_INFO_ITEM_RIGHT
            )
            if pairs_ratio > 0.75:
                _info_container_mark(parent)


def find(soup):
    """Find items pairs and then check if holder is container or his parent."""
    holders = get_holders(soup)
    for holder in holders:
        _info_items_pairs(holder)

    # List of left side items for finding containers
    left_items = soup.find_all(semantic_role=SEM_ROLE_INFO_ITEM_LEFT)
    _container_process(left_items)
