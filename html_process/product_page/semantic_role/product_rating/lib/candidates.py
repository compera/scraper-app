"""Find product rating candidates."""
from html_process.page.semantic_type import (
    semantic_type
)
from html_process.common.utils import (
    is_number,
)
from html_process.page.tag.tag_clickable import tag_clickable
from html_process.page.tag.tag_position import tag_position
from html_process.common.app_var import (
    SEMANTIC_ROLE_CANDIDATE,
    SEM_ROLE_CANDIDATE_RATING,
    SEM_ROLE_CANDIDATE_RATING_CONTAINER,
)


def _candidate_rating_mark(tag):
    """Mark product rating candidate."""
    tag[SEMANTIC_ROLE_CANDIDATE] = SEM_ROLE_CANDIDATE_RATING
    tag["style"] = "border: pink 1px solid !important"


def _candidate_container_mark(tag):
    """Mark product container candidate."""
    tag[SEMANTIC_ROLE_CANDIDATE] = SEM_ROLE_CANDIDATE_RATING_CONTAINER
    tag["style"] = "border: pink 1px solid !important"


def _is_float_number(number):
    """Split the number by . and check if number is float."""
    number_split = number.split(".")

    return (
        is_number(number_split[0]) if
        len(number_split) > 1
        else False
    )


def _is_slash_number(num_word):
    is_number_check = False
    is_less_equal_to_five = False
    num_word_split = num_word.split("/")
    if len(num_word_split) > 1:
        is_number_check = is_number(num_word_split[0])
    if is_number_check:
        is_less_equal_to_five = float(num_word_split[0]) <= 5

    return (
        is_number_check and is_less_equal_to_five
    )


def _number_and_word_check(num_word):
    """Check the first word of the string if float."""
    num_word_split = num_word.split(" ")
    return _is_float_number(num_word_split[0])


def _elements_related(tag_container, src_pos):
    """Check if all elements inside a container related pos."""
    all_related = True
    src_top = src_pos["top"]

    for child in tag_container.children:
        child_pos = tag_position.this_get(
            tag_position.find_parent_with_pos(child)
        )
        child_top = child_pos["top"]
        if(child_top > 0 and abs(src_top - child_top) > 20):
            all_related = False
            break

    return all_related


def _walker_up(tag, src_tag, src_tag_pos):
    """Check tag main container if can be product rating container."""
    if tag and tag.name is not None:

        related_children = _elements_related(tag, src_tag_pos)

        is_candidate_container = False

        # container template
        if(related_children):
            elements_count = 0
            clickable_elements_count = 0
            for child in tag.children:
                if child.name is not None:
                    elements_count += 1
                    for desce in child.descendants:
                        if desce.name is not None:
                            if tag_clickable.has_clickable_attr(desce):
                                clickable_elements_count += 1
                                break

            if(elements_count > 1 and clickable_elements_count >= 1):
                is_candidate_container = True

            if(is_candidate_container):
                _candidate_rating_mark(src_tag)
                _candidate_container_mark(tag)
            else:
                _walker_up(tag.parent, src_tag, src_tag_pos)


def _candidates(soup):
    """
    Find product rating and product rating container.

    Start from product rating and go up to see if his container,
    can be product rating container.
    """
    # Fining candidates between number type
    for tag in semantic_type.get_all_numbers(soup):
        if tag.string is not None and _is_float_number(tag.string.strip()):
            src_pos = tag_position.this_get(
                tag_position.find_parent_with_pos(tag)
            )
            _walker_up(tag, tag, src_pos)

    # Fining candidates between number and word type
    for tag in semantic_type.get_all_number_and_word(soup):
        number_word_check = _number_and_word_check(tag.string.strip())
        number_slash_check = _is_slash_number(tag.string.strip())
        if (number_word_check or number_slash_check):
            src_pos = tag_position.this_get(
                tag_position.find_parent_with_pos(tag)
            )
            _walker_up(tag, tag, src_pos)


def find(soup):
    """Find product rating."""
    _candidates(soup)
