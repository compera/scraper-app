"""Find product rating and product rating container role."""
from html_process.page.tag.tag_position import tag_position
from html_process.common.app_var import (
    SEMANTIC_ROLE,
    SEM_ROLE_RATING,
    SEM_ROLE_RATING_CONTAINER,
    SEM_ROLE_CANDIDATE_RATING,
)


def _role_rating_mark(tag):
    """Mark product rating candidate."""
    tag[SEMANTIC_ROLE] = SEM_ROLE_RATING
    tag["style"] = "border: pink 3px solid !important"


def _role_container_mark(tag):
    """Mark product container candidate."""
    tag[SEMANTIC_ROLE] = SEM_ROLE_RATING_CONTAINER
    tag["style"] = "border: pink 3px solid !important"


def _process_number_of_candidates(all_candidates):
    """Choose role by the topest element.

    TODO:  need more work to make it smarter.
    """
    best_top = -1
    top_element = ""
    for candi in all_candidates:
        this_pos = tag_position.this_get(candi)
        this_top = this_pos["top"]
        if(this_top < best_top or best_top == -1):
            best_top = this_top
            top_element = candi

    return top_element


def find(all_candidates):
    """Find."""
    if len(all_candidates) == 1:
        container = all_candidates[0]
        if(container is None or isinstance(container, str)):
            return

        _role_container_mark(container)
        rating = container.find(semantic_role_candidate=(
            SEM_ROLE_CANDIDATE_RATING
        ))
        _role_rating_mark(rating)
    else:
        print("TODO: imporve product rating handle number of candidates")
        container = _process_number_of_candidates(all_candidates)
        if(container is None or isinstance(container, str)):
            return

        _role_container_mark(container)
        rating = container.find(semantic_role_candidate=(
            SEM_ROLE_CANDIDATE_RATING
        ))
        _role_rating_mark(rating)
