"""Product rating."""
from html_process.product_page.semantic_role.product_rating.lib import (
    candidates,
    role
)
from html_process.common.app_var import (
    SEMANTIC_TYPE,
    SEM_TYPE_NUMBER,
    SEM_TYPE_NUMBER_WORD,
    SEMANTIC_ROLE_CANDIDATE,
    SEM_ROLE_CANDIDATE_RATING_CONTAINER,
    SEMANTIC_ROLE,
    SEM_ROLE_RATING,
)


def product_rating_extract(soup):
    """Return product rating."""
    rating = "not found"
    rating_tag = soup.find(semantic_role=SEM_ROLE_RATING)
    if rating_tag:
        s_type = rating_tag[SEMANTIC_TYPE]
        if(s_type == SEM_TYPE_NUMBER):
            rating = rating_tag.string.strip()
        elif(s_type == SEM_TYPE_NUMBER_WORD):
            rating_split = rating_tag.string.strip().split(" ")
            rating = rating_split[0]

    return rating


def find(soup):
    """Product rating."""
    # Find candidates
    candidates.find(soup)

    # Get all candidates containers
    candidates_containers = soup.find_all(semantic_role_candidate=(
        SEM_ROLE_CANDIDATE_RATING_CONTAINER
    ))
    # Find role
    role.find(candidates_containers)
