"""Find main container role."""
from html_process.product_page.semantic_role.semantic_role import (
    get_product_title_tag,
    get_product_price_tag,
    get_product_image_tag
)
from html_process.common.app_var import (
    SEMANTIC_ROLE,
    SEM_ROLE_MAIN_CONTAINER,
)


def _walker_up(parent):
    """Walker, start from title tag and go up to find other semantic roles."""
    if(parent is not None and parent.name is not None):
        contain_title = get_product_title_tag(parent)
        contain_price = get_product_price_tag(parent)
        contain_image = get_product_image_tag(parent)

        if(contain_title and contain_price and contain_image):
            parent[SEMANTIC_ROLE] = SEM_ROLE_MAIN_CONTAINER
            parent["style"] = "border: black 3px solid"
        else:
            _walker_up(parent.parent)


def find(soup):
    """Find main container role."""
    title_tag = get_product_title_tag(soup)

    _walker_up(title_tag)
