"""Product variations."""
import re
from html_process.product_page.semantic_role.product_variations.lib import (
    clickable_variations,
    on_page_variations,
    on_page_variations_data_extraction,
)
from html_process.common.app_var import (
    SEM_ROLE_MAIN_CONTAINER,
)


def find(soup, product_model_variation):
    """Using two methods."""
    soup_main_container = soup.find(
        semantic_role=re.compile(SEM_ROLE_MAIN_CONTAINER)
    )
    clickable_variations.find(soup_main_container)

    if(len(product_model_variation) > 0):
        on_page_variations.find(
            soup,
            soup_main_container,
            product_model_variation
        )


def get_clickable_items(soup):
    """Warpper."""
    return clickable_variations.get_clickable_items(soup)


def on_page_data_extraction(soup):
    """"Data extraction warpper."""
    return on_page_variations_data_extraction.extract(soup)
