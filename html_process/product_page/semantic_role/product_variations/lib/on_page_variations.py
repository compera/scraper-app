"""On page variations method. using previous knowledge from product model."""
import re
from html_process.page.tag.tag_position import tag_position
from html_process.common.app_var import (
    SEM_ROLE_CANDIDATE_PRICE,
    SEMANTIC_ROLE_CANDIDATE,
    SEM_ROLE_CANDIDATE_VARIATION_ON_PAGE_CONTAINER,
    SEM_ROLE_CANDIDATE_VARIATION_ON_PAGE,
    VARIATION_LABEL,
)


def _fix_tag(main_soup, variation_label, variation_string, element_tag):
    """Split variation name and price if they combined to differnet tags.

    Keyword arguments:
    main_soup -- main_soup object
    variation_label - item from variation list
    variation_string - The string contain the variation label
    element_tag - The element holding the variation string
    """
    variation = variation_string.strip().replace("-", "").replace("/", "")
    variation_split = variation.split(variation_label)

    is_in_same_tag = False
    variation_strings = []
    for index, vari in enumerate(variation_split):
        word = vari.strip()
        if(word.startswith("$")):
            variation_strings.insert(index, str(word))
            is_in_same_tag = True
        else:
            variation_strings.insert(index, str(word) + str(variation_label))

    if(is_in_same_tag):
        for vari in variation_strings:
            if(vari.startswith("$")):
                price_tag = main_soup.new_tag(
                    "span",
                    alfred_tag="self",
                    semantic_role_candidate=SEM_ROLE_CANDIDATE_PRICE
                )
                price_tag.string = vari
                element_tag.insert_after(price_tag)
            else:
                type_tag = main_soup.new_tag("span", alfred_tag="self")
                type_tag.string = vari
                element_tag.insert_before(type_tag)
        element_tag.decompose()


def _related_by_pos(pos_top1, pos_top2):
    """Check if two element are related by checking the position top."""
    return (abs(pos_top1 - pos_top2) <= 20)


def _mark_candidate_variation_on_page(tag, variation_label):
    """Mark variation element and add variation label."""
    tag[SEMANTIC_ROLE_CANDIDATE] = SEM_ROLE_CANDIDATE_VARIATION_ON_PAGE
    tag[VARIATION_LABEL] = variation_label
    tag["style"] = "border: yellow 1px solid"


def _walker_up_variation_element(soup, _variation_tag, variation_label):
    """
    Find variation element candidates.

    check if variation tag and price tag are related.
    """
    if(soup and soup.name is not None):
        contain_price = soup.find(
            semantic_role_candidate=(SEM_ROLE_CANDIDATE_PRICE)
        )

        if(contain_price is not None):
            price_tag = tag_position.find_parent_with_pos(contain_price)
            price_tag_pos = tag_position.this_get(price_tag)
            price_tag_pos_top = price_tag_pos["top"]

            variation_tag = tag_position.find_parent_with_pos(_variation_tag)
            variation_tag_pos = tag_position.this_get(variation_tag)
            variation_tag_pos_top = variation_tag_pos["top"]

            related = _related_by_pos(
                price_tag_pos_top,
                variation_tag_pos_top
            )

            if(related):
                _mark_candidate_variation_on_page(soup, variation_label)
        else:
            _walker_up_variation_element(
                soup.parent,
                _variation_tag,
                variation_label
            )


def _variation_element_candidates(main_soup, soup, variation_list):
    """Find variation element candidates."""
    # fix variation tags if necessery

    for variation_label in variation_list:
        options = soup.find_all(string=re.compile(variation_label))

        for variation in options:
            _fix_tag(main_soup, variation_label, variation, variation.parent)

    # walker
    for variation_label in variation_list:
        options = soup.find_all(string=re.compile(variation_label))
        for variation in options:
            _walker_up_variation_element(
                variation.parent,
                variation.parent,
                variation_label
            )


def _mark_candidate_container(tag, variation_label):
    """Mark variation container and add variation label."""
    tag[SEMANTIC_ROLE_CANDIDATE] = (
        SEM_ROLE_CANDIDATE_VARIATION_ON_PAGE_CONTAINER
    )
    tag[VARIATION_LABEL] = variation_label
    tag["style"] = "border: yellow 3px solid"


def _walker_up_variation_container(soup):
    """
    Find variation container candidates.

    check if variation tag and price tag are related.
    """
    if(soup and soup.name is not None):
        container_variation_elements = soup.find_all(
            semantic_role_candidate=(
                SEM_ROLE_CANDIDATE_VARIATION_ON_PAGE
            )
        )

        if(len(container_variation_elements) > 1):
            variation_label = container_variation_elements[0][VARIATION_LABEL]
            _mark_candidate_container(soup, variation_label)

        else:
            _walker_up_variation_container(soup.parent)


def _variation_container_candidates(soup):
    """Find variation container candidates."""
    variation_tags = soup.find_all(
        semantic_role_candidate=(
            SEM_ROLE_CANDIDATE_VARIATION_ON_PAGE
        )
    )
    for tag in variation_tags:
        _walker_up_variation_container(tag)


def find(main_soup, soup, variation_list):
    """Find on page variation.

    Keyword arguments:
    main_soup -- main_soup object
    soup - soup main container object
    variation_list - list of collected variation names from product model
    """

    _variation_element_candidates(main_soup, soup, variation_list)
    _variation_container_candidates(soup)
