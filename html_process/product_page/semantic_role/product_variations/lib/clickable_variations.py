"""Clickable variations method."""
from html_process.common import selector
from html_process.page.tag.tag_clickable.tag_clickable import (
    has_clickable_attr
)
from html_process.page.tag.tag_types.tag_type import (
    tag_is_leaf_or_holder,
    is_leaf_chain,
    is_leaf,
)
from html_process.common.app_var import (
    SEMANTIC_TYPE,
    SEM_TYPE_NUMBER,
    SEMANTIC_ROLE_CANDIDATE,
    SEM_ROLE_CANDIDATE_VARIATION_CLICKABLE_CONTAINER,
    SEM_ROLE_CANDIDATE_VARIATION_CLICKABLE,
)


def _variation_container_candidate_mark(tag):
    """Mark variation candidate."""
    tag[SEMANTIC_ROLE_CANDIDATE] = (
        SEM_ROLE_CANDIDATE_VARIATION_CLICKABLE_CONTAINER
    )
    tag["style"] = "border: 1px yellow solid"


def _variation_child_candidate_mark(container):
    """Mark all children for semantic_role_candidate."""
    for child in container:
        if child.name is not None:
            child[SEMANTIC_ROLE_CANDIDATE] = (
                SEM_ROLE_CANDIDATE_VARIATION_CLICKABLE
            )
            child["style"] = "border: 3px yellow solid"


def _leaf_chain_leaf_is_img(tag):
    tag_name = ""
    for desc in tag.descendants:
        if desc.name is not None:
            if is_leaf(desc):
                tag_name = desc.name
    return tag_name == "img"


def _similiar_elements(holder):
    """Return true if holder has similiar elements."""
    similiar_element_name = ""
    similiar_element_count = 0

    for child in holder:
        if(child.name is not None and child.has_attr("alfred_tag") is False):
            if(tag_is_leaf_or_holder(child) and
               _leaf_chain_leaf_is_img(child) is False):
                if(similiar_element_name == ""):
                    similiar_element_name = child.name
                    similiar_element_count += 1
                elif(similiar_element_name == child.name):
                    similiar_element_count += 1
                elif(similiar_element_name != child.name):
                    similiar_element_count = 0
                    break

    return similiar_element_count > 1


def _variation_container_candidate(holder):
    """Return true if holder can be variation container."""
    clickable_elements = 0
    number_of_elements = 0
    leaf_holder_elements = 0
    for tag in holder:
        if(tag.name is not None):
            number_of_elements += 1

            if(tag_is_leaf_or_holder(tag)):
                leaf_holder_elements += 1
                is_clickable_element = has_clickable_attr(tag)
                leaf_semantic_type = ""
                # tag descendants -
                # check if any descendants is clickable and leaf is not number
                for desce in tag.descendants:
                    if(desce.name is not None):
                        valid_name = (
                            desce.name != "a" and
                            desce.name != "input"
                        )
                        if(has_clickable_attr(desce) and valid_name):
                            is_clickable_element = True
                            leaf_semantic_type = (
                                desce[SEMANTIC_TYPE] if
                                desce.has_attr(SEMANTIC_TYPE)
                                else ""
                            )

                sem_type_valid = leaf_semantic_type != SEM_TYPE_NUMBER
                if(is_clickable_element and sem_type_valid):
                    clickable_elements += 1

    contains_only_leaf_holder = number_of_elements - leaf_holder_elements == 0

    return (
        contains_only_leaf_holder and
        abs(leaf_holder_elements - clickable_elements) <= 1
    )


def _walker(soup):
    if soup and soup.name is not None:
        for child in soup.children:
            if(child.name is not None):
                similiar_holder = _similiar_elements(child)
                if(similiar_holder):
                    is_candidate = _variation_container_candidate(child)
                    if is_candidate:
                        _variation_container_candidate_mark(child)
                        _variation_child_candidate_mark(child)

            _walker(child)


def find(main_container_soup):
    """Clickable variation start."""
    _walker(main_container_soup)


def get_clickable_items(soup):
    """Return object of clickbale items for further process."""
    product_variation_clickable_items = soup.find_all(
        semantic_role_candidate=(
            SEM_ROLE_CANDIDATE_VARIATION_CLICKABLE
        )
    )

    clickable_items = {}
    for clickable in product_variation_clickable_items:
        item_id = selector.xpath(clickable.parent)

        if item_id in clickable_items:
            clickable_items[item_id].append(selector.xpath(clickable))
        else:
            clickable_items[item_id] = []
            clickable_items[item_id].append(selector.xpath(clickable))

    return clickable_items
