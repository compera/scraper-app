"""Extract variation with correct format."""
# from html_process.common import selector
import unicodedata
import re

from html_process.common.app_var import (
    SEM_ROLE_CANDIDATE_VARIATION_ON_PAGE_CONTAINER,
    SEM_ROLE_CANDIDATE_VARIATION_ON_PAGE,
    VARIATION_LABEL,
    SEM_ROLE_CANDIDATE_PRICE
)


def extract(soup):
    """Extract variation with correct format."""
    product_variation = {}
    all_containers = soup.find_all(
        semantic_role_candidate=(
            SEM_ROLE_CANDIDATE_VARIATION_ON_PAGE_CONTAINER
        )
    )

    if(len(all_containers) == 0):
        return product_variation

    for i, container in enumerate(all_containers):
        all_elements = container.find_all(
            semantic_role_candidate=(
                SEM_ROLE_CANDIDATE_VARIATION_ON_PAGE
            )
        )

        # info
        # product_variation["model"] = "TODO"

        product_variation["list"] = []
        variation_type = container[VARIATION_LABEL]
        product_variation["type"] = variation_type

        for element in all_elements:
            price_tag = element.find(
                semantic_role_candidate=(SEM_ROLE_CANDIDATE_PRICE)
            )
            price = price_tag.string.strip()
            variation_tag_string = element.find(
                string=re.compile(variation_type)
            )
            variation = variation_tag_string.strip()

            product_variation["list"].append({
                "title": "",
                "price": unicodedata.normalize("NFKD", price),
                "variation": unicodedata.normalize("NFKD", variation)
            })

    return product_variation
