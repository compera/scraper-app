"""Semantic role."""
from html_process.page.tag.tag_types.tag_type import (
    is_leaf,
    is_leaf_chain,
    is_holder
)
from html_process.common.utils import (
    is_number,
)
from html_process.common.app_var import (
    # SEMANTIC_ROLE,
    SEM_ROLE_TITLE,
    SEM_ROLE_PRICE,
    SEM_ROLE_IMAGE,
)


def get_product_title_tag(soup):
    """Return produc title element."""
    return soup.find(semantic_role=(SEM_ROLE_TITLE))


def get_product_price_tag(soup):
    """Return produc price element."""
    return soup.find(semantic_role=(SEM_ROLE_PRICE))


def get_product_image_tag(soup):
    """Return produc image element."""
    return soup.find(semantic_role=(SEM_ROLE_IMAGE))


def product_image_extract_src(soup):
    """Extract images url."""
    src = get_product_image_tag(soup)
    return (
      src["src"] if src.has_attr("src") else False
    )


def product_title_extract(soup):
    """Product title string."""
    return get_product_title_tag(soup).string.strip()


def product_price_extract(soup):
    """Product price string."""
    price_tag = get_product_price_tag(soup)
    price_string = ""

    if price_tag.string is not None and len(price_tag.string) != 1:
        price_string = price_tag.string.strip()
    else:
        for string in price_tag.strings:
            price_string += string

    return_ind = False
    if len(price_string) == 1:
        # try silibing tag
        if price_tag.next_sibling:
            if price_tag.next_sibling.string and is_number(price_tag.next_sibling.string):
                price_string += price_tag.next_sibling.string
                return_ind = True

        tag_parent = price_tag.parent
        sibling_tag = tag_parent.next_sibling
        if return_ind is False and sibling_tag:
            if is_leaf_chain(sibling_tag):
                for desc in sibling_tag.descendants:
                    if desc.name is not None:
                        if is_leaf(desc):
                            sibling_tag = desc

        if return_ind is False and sibling_tag.string:
            if is_number(sibling_tag.string.strip().replace(',', '')):
                price_string += sibling_tag.string.strip()

    return price_string
