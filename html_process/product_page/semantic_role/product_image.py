"""Find product image role."""
from html_process.product_page.semantic_role.semantic_role import (
    get_product_title_tag,
)
from html_process.page.tag.tag_position import tag_position
from html_process.common.app_var import (
    SEMANTIC_ROLE,
    SEM_ROLE_IMAGE,
)


def _find_all_img_and_canvas(tag):
    """Return true if tag name fit."""
    return tag.name == "img" or tag.name == "canvas"


def _candidates(soup):
    """Find all images tags."""
    return soup.find_all(_find_all_img_and_canvas)


def _score(pos_top, pos_width, pos_height, title_pos_top):
    """Position method score."""
    if(pos_top == 0):
        return 0
    # negative facor if image is rectangle like shape
    rec_factor_param = 0.3 if pos_width > pos_height else 1
    # bonus if image is close to title pos
    bonus_title_param = 2 if abs(pos_top - title_pos_top) < 50 else 1

    return (
        ((pos_width * pos_height) * rec_factor_param * bonus_title_param) /
        pos_top
    )


def find(soup):
    """Find product image."""
    all_images = _candidates(soup)
    current_img = None
    current_score = -1

    # give bonus point if image is in title area
    title_tag = get_product_title_tag(soup)
    title_pos = tag_position.this_get(title_tag)
    title_pos_top = title_pos["top"]

    for img in all_images:
        if(tag_position.has_pos_attr(img) is False):
            return

        this_pos = tag_position.this_get(img)
        this_score = _score(
            this_pos["top"],
            this_pos["width"],
            this_pos["height"],
            title_pos_top
        )

        if(current_img is None):
            current_img = img
            current_score = this_score
            current_img[SEMANTIC_ROLE] = SEM_ROLE_IMAGE
            current_img["style"] = "border: red 3px solid !important"

        elif(this_score > current_score):
            current_img[SEMANTIC_ROLE] = ""
            current_img["style"] = ""

            current_score = this_score
            img[SEMANTIC_ROLE] = SEM_ROLE_IMAGE
            img["style"] = "border: red 3px solid !important"
            current_img = img
