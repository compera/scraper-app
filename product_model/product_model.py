"""import Path."""
from pathlib import Path
import json


def load_model(model_name):
    """Load product model file.

    Keyword arguments:
    model_name -- the name of the model
    """
    product_model_dir = Path('./data/product models')
    model_filename = product_model_dir.joinpath(model_name + '.json')
    product_model = {}

    if(model_filename.exists()):
        product_model = json.loads(model_filename.read_text())

    return product_model


def export_model(model, model_name):
    """Export product model file.

    Keyword arguments:
    model -- the model object
    model_name -- the name of the model
    """
    product_model_dir = Path('./data/product models')
    model_filename = product_model_dir.joinpath(model_name + '.json')

    with model_filename.open("w") as f:
        f.write(json.dumps(model, indent=4))


def check_if_variation_is_valid(results):
    """Check if variation object is valid.

    Keyword arguments:
    results -- the variation object
    """
    price_variation_counter = len(list(results.keys()))
    price_holder = []

    for key in results:
        price_holder.append(results[key]["price"])

    price_variation_set = set(price_holder)
    return (
        True if (
            len(price_holder) > 1 and
            price_variation_counter > 0 and
            price_variation_counter - len(price_variation_set) < 1
        )
        else False
    )


def _get_variation_type(results):
    """Extract repeated variation type(word) from the variation object.

    Keyword arguments:
    results -- the variation object
    """
    variation_type_dict = {}
    results_number = len(results.keys())

    for key in results:
        variation_name = results[key]["variation"]
        variation_name_norm = (
            variation_name.replace("-", " ").replace("(", "").replace(")", "")
        )
        vari_split = variation_name_norm.split(" ")
        for i, w in enumerate(vari_split):
            word = w.strip()
            if word in variation_type_dict:
                variation_type_dict[word]["counter"] += 1
            else:
                variation_type_dict[word] = {}
                variation_type_dict[word]["counter"] = 1
                variation_type_dict[word]["pos"] = i

    variation_type_options = []
    for word_key in variation_type_dict:
        if(variation_type_dict[word_key]["counter"] == results_number):
            variation_type_options.append(word_key)

    variation_type = "not found"

    if(len(variation_type_options) > 0):
        variation_type = " ".join(variation_type_options)

    return variation_type


def variation_normalize(results):
    """Structure the results getting from the cralwer to preferd structure.

    Keyword arguments:
    results -- the variation object
    """
    normalized = {}
    normalized["info"] = {}
    normalized["info"]["total"] = len(results.keys())
    for key in results:
        normalized[str(key)] = {}
        normalized[str(key)]["price"] = results[key]["price"].strip()
        normalized[str(key)]["title"] = results[key]["title"].strip()
        variation_name = results[str(key)]["variation"]
        variation_name_norm = (
            variation_name.replace("-", " ").replace("(", "").replace(")", "")
        )
        normalized[str(key)]["variation"] = variation_name_norm

    normalized["info"]["type"] = _get_variation_type(results)

    return normalized


def model_variation(model, result):
    """Create new model from the variation results and merge variation types.

    Keyword arguments:
    results -- the variation object
    """
    print("model")
    print(model)
    print("*** result ***")
    print(result)
    result_type = result["info"]["type"]
    result_names_list = []
    for key in result:
        if(key != "info"):
            result_names_list.append(result[key]["variation"])

    print("*** result_names_list ***")
    print(result_names_list)
    # if model is empty
    if(len(model["variations"].keys()) == 0):
        model["variations"][result_type] = result_names_list
    else:
        result_type_splitted = result_type.split(" ")
        for key in model["variations"].copy():
            # in case exact match
            if(key == result_type):
                model["variations"][key] += result_names_list
            # in case partial match
            else:
                key_split = key.split(" ")
                match = False
                for k in key_split:
                    for r in result_type_splitted:
                        if(k == r):
                            model["variations"][r] = (
                                model["variations"][key] + result_names_list
                            )
                            del model["variations"][key]
                            match = True
                            break
                # in case no match at all
                if(match is False):
                    model["variations"][result_type] = result_names_list

    return model["variations"]
