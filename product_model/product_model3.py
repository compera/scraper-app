"""import Path."""
# from pathlib import Path
import pymongo
from scrapy.conf import settings
import json
from collections import Counter
import re


def load_model(model_id):
    """Load product model from db.

    Keyword arguments:
    model_id -- the model id
    """
    connection = pymongo.MongoClient(
        settings['MONGODB_SERVER'],
        settings['MONGODB_PORT']
    )

    db = connection[settings['MONGODB_DB']]
    model_collection = db["product_models"]
    model = model_collection.find_one({"_id": model_id})

    if model is None:
        model = {
            "_id": model_id,
            "variations": {},
            "variationsXpathKeys": {}
        }
        model_collection.insert_one(model)

    connection.close()

    return model


def update_model_vari_obj(product_model, vari_obj):
    """Update product model.

    Keyword arguments:
    model_id -- the model id
    """
    connection = pymongo.MongoClient(
        settings['MONGODB_SERVER'],
        settings['MONGODB_PORT']
    )

    db = connection[settings['MONGODB_DB']]
    model_collection = db["product_models"]

    pm_variations = product_model["variations"]

    model_vari_id = vari_obj["model_vari_id"]
    print("****** update_model ******")
    print("** vari_obj **")
    print(vari_obj)
    new_varis_list = []

    for v in vari_obj["list"]:
        if v["variation"] != 'undefined':
            new_varis_list.append(v["variation"])

    print("** new_varis_list **")
    print(new_varis_list)
    print("** pm_variations **")
    print(pm_variations)
    if len(pm_variations.keys()) and pm_variations[str(model_vari_id)]:
        new_varis_list = list(set(
            new_varis_list + pm_variations[str(model_vari_id)]["vari_list"]
        ))

    vairs_counter = _get_variation_type_counter(new_varis_list)
    vari_type = _get_variation_type(vairs_counter)
    update_vari_obj = {
        "is_valid": (
            pm_variations[str(model_vari_id)]["is_valid"] if
            len(pm_variations.keys()) and pm_variations[str(model_vari_id)]["is_valid"] else True
        ),
        "vari_list": new_varis_list,
        "type": vari_type
    }
    model_collection.update_one(
        {"_id": product_model["_id"]},
        {
            "$set": {"variations." + str(model_vari_id): update_vari_obj}
        },
    )

    connection.close()


def update_model_vari_xpath_keys(product_model, key, store, is_valid):
    """Update product model.

    Keyword arguments:
    model_id -- the model id
    """
    connection = pymongo.MongoClient(
        settings['MONGODB_SERVER'],
        settings['MONGODB_PORT']
    )

    db = connection[settings['MONGODB_DB']]
    model_collection = db["product_models"]

    xpathKeyObj = {
        "store": str(store),
        "is_valid": bool(is_valid)
    }

    model_collection.update_one(
        {"_id": product_model["_id"]},
        {
            "$set": {"variationsXpathKeys." + str(key): xpathKeyObj}
        },
    )

    connection.close()


def get_variations_keys(model):
    "Get variations keys."
    varis_types_keys = []
    for vari_key in model["variations"]:
        vari_obj = model["variations"][vari_key]
        is_valid = vari_obj["is_valid"]
        if bool(is_valid):
            vari_counter = _get_variation_type_counter(vari_obj["vari_list"])
            vari_type = _get_variation_type(vari_counter)
            varis_types_keys.append(vari_type)
            # shoretest_word_key = ""
            # shoretest_num_of_words = 0
            # for word_key in var_obj["key_list"]:
            #     num_of_words = len(word_key.split(" "))
            #     if shoretest_num_of_words == 0:
            #         shoretest_word_key = word_key
            #         shoretest_num_of_words = num_of_words

            #     if num_of_words < shoretest_num_of_words:
            #         shoretest_word_key = word_key
            #         shoretest_num_of_words = num_of_words

            #     varis.append(shoretest_word_key)

    return varis_types_keys


# def export_model(model, model_name):
#     """Export product model file.

#     Keyword arguments:
#     model -- the model object
#     model_name -- the name of the model
#     """
#     product_model_dir = Path('./data/product models')
#     model_filename = product_model_dir.joinpath(model_name + '.json')

#     with model_filename.open("w") as f:
#         f.write(json.dumps(model, indent=4))


def variation_is_valid(results):
    """Check if variation object is valid.

    Keyword arguments:
    results -- the variation object
    """
    price_variation_counter = len(list(results.keys()))
    price_holder = []

    for key in results:
        price_holder.append(results[key]["price"])

    price_variation_set = set(price_holder)
    return (
        True if (
            len(price_holder) > 1 and
            price_variation_counter > 0 and
            price_variation_counter - len(price_variation_set) < 1
        )
        else False
    )


def variation_normalize(results, product_model):
    """Structure the results getting from the cralwer to preferd structure.

    Keyword arguments:
    results -- the variation object
    """

    variation = {
        "model_vari_id": "",
        # "vari_type": "",
        "list": []
    }

    vari_types_list = []
    vari_list = []

    for key in results:
        # normalize variation type
        vari = (
            results[key]["variation"]
            .replace("-", " ")
            .replace("(", "")
            .replace(")", "")
        )

        if (vari != 'undefined'):
            vari_types_list.append(vari)

        vari_list.append({
            'price': results[key]["price"],
            'title': results[key]["title"],
            'variation': vari
        })

    variation["list"] = vari_list
    # variation["vari_type"] = _get_variation_type(vari_types_list)

    # pm_vari_keys = product_model["variations"].keys()
    # model_vari_id = ""

    # if len(pm_vari_keys) == 0:
    #     model_vari_id = 1
    # else:
    model_vari_id = 1
    for key in product_model["variations"]:
        this_vari = product_model["variations"][key]

        if bool(this_vari["is_valid"]) is False:
            pass

        should_count = len(this_vari["vari_list"])
        vari_counter = _get_variation_type_counter(
            this_vari["vari_list"] + vari_types_list
        )
        this_count = vari_counter.most_common(1)[0][1]
        if this_count == should_count:
            model_vari_id = key

    variation["model_vari_id"] = model_vari_id

    return variation


def _get_variation_type(words_counter):
    most_common = words_counter.most_common(1)[0][1]
    variation_words_list = []
    for word_c in words_counter.items():
        if word_c[1] == most_common:
            variation_words_list.append(word_c[0])

    return " ".join(variation_words_list)


def _get_variation_type_counter(vars_list):
    """Extract repeated variation type(word) from the variation types list.

    Keyword arguments:
    vars_list -- list of variations types
    """
    var_list_joind = ' '.join(vars_list)
    reg = re.compile('\S{1,}')
    # words_counter = Counter(ma.group() for ma in reg.finditer(var_list_joind))
    # most_common = words_counter.most_common(1)[0][1]
    # variation_words_list = []
    # for word_c in words_counter.items():
    #     if word_c[1] == most_common:
    #         variation_words_list.append(word_c[0])

    # return " ".join(variation_words_list)
    return Counter(ma.group() for ma in reg.finditer(var_list_joind))
# def variation_normalize(results):
#     """Structure the results getting from the cralwer to preferd structure.

#     Keyword arguments:
#     results -- the variation object
#     """
#     normalized = {}
#     normalized["info"] = {}
#     normalized["info"]["total"] = len(results.keys())
#     for key in results:
#         normalized[str(key)] = {}
#         normalized[str(key)]["price"] = results[key]["price"].strip()
#         normalized[str(key)]["title"] = results[key]["title"].strip()
#         variation_name = results[str(key)]["variation"]
#         variation_name_norm = (
#             variation_name.replace("-", " ").replace("(", "").replace(")", "")
#         )
#         normalized[str(key)]["variation"] = variation_name_norm

#     normalized["info"]["type"] = _get_variation_type(results)

#     return normalized


# def model_variation(model, result):
#     """Create new model from the variation results and merge variation types.

#     Keyword arguments:
#     results -- the variation object
#     """
#     print("model")
#     print(model)
#     print("*** result ***")
#     print(result)
#     result_type = result["info"]["type"]
#     result_names_list = []
#     for key in result:
#         if(key != "info"):
#             result_names_list.append(result[key]["variation"])

#     print("*** result_names_list ***")
#     print(result_names_list)
#     # if model is empty
#     if(len(model["variations"].keys()) == 0):
#         model["variations"][result_type] = result_names_list
#     else:
#         result_type_splitted = result_type.split(" ")
#         for key in model["variations"].copy():
#             # in case exact match
#             if(key == result_type):
#                 model["variations"][key] += result_names_list
#             # in case partial match
#             else:
#                 key_split = key.split(" ")
#                 match = False
#                 for k in key_split:
#                     for r in result_type_splitted:
#                         if(k == r):
#                             model["variations"][r] = (
#                                 model["variations"][key] + result_names_list
#                             )
#                             del model["variations"][key]
#                             match = True
#                             break
#                 # in case no match at all
#                 if(match is False):
#                     model["variations"][result_type] = result_names_list

#     return model["variations"]
